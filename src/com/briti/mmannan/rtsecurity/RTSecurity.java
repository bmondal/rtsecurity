package com.briti.mmannan.rtsecurity;

import java.io.FileOutputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.Member;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Binder;
import android.os.Build;
import android.os.Process;
import android.util.Log;
import de.robv.android.xposed.IXposedHookZygoteInit;
import de.robv.android.xposed.IXposedHookLoadPackage;
import de.robv.android.xposed.callbacks.XC_LoadPackage.LoadPackageParam;
import de.robv.android.xposed.XposedBridge;
import de.robv.android.xposed.XposedHelpers;
import de.robv.android.xposed.XC_MethodHook;
import static de.robv.android.xposed.XposedHelpers.findClass;

@SuppressLint("DefaultLocale")
public class RTSecurity implements IXposedHookLoadPackage, IXposedHookZygoteInit {
	private static String mSecret = null;

	private static boolean mLocationManagerHooked = false;
	private static boolean mSensorManagerHooked = false;
	private static boolean mAccountManagerHooked = false;
	private static boolean mWiFiManagerHooked = false;
	private static boolean mConnectivityHooked = false;
	private static List<String> mListHookError = new ArrayList<String>();


	@SuppressLint("InlinedApi")
	public void initZygote(StartupParam startupParam) throws Throwable {
		
		mSecret = Long.toHexString(new Random().nextLong());

		try {

			Class<?> cSystemServer = Class.forName("com.android.server.SystemServer");
			Method mMain = cSystemServer.getDeclaredMethod("main", String[].class);
			XposedBridge.hookMethod(mMain, new XC_MethodHook() {
				@Override
				protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
					
				}
			});
		} catch (Throwable ex) {
			
		}
		
		hookAll(RTScameras.getInstances(), mSecret);
		// Audio record
		hookAll(RTSmicrophone.getInstances(), mSecret);
		
		hookAll(RTSmediarec.getInstances(), mSecret);
		hookAll(RTSmobileNetwork.getInstances(), mSecret);
		
		hookAll(RTSappActivity.getInstances(), mSecret);
		hookAll(RTScontact.getInstances(), mSecret);
		hookAll(RTSnfc.getInstances(), mSecret);
		hookAll(RTSbluetooth.getInstances(),mSecret);
		hookAll(RTSsms.getInstances(),mSecret);
		hookAll(RTSstorage.getInstances(),mSecret);
		hookAll(RTSwidget.getInstances(), mSecret);
		hookAll(RTSdeviceid.getInstances(), mSecret);
			
	}

	public void handleLoadPackage(final LoadPackageParam lpparam) throws Throwable {

		try {
			Class.forName("com.google.android.gms.location.LocationClient", false, lpparam.classLoader);
			hookAll(RTSlocation.getInstances(), lpparam.classLoader, mSecret);
		} catch (Throwable ignored) {
		}
	}

	public static void handleGetSystemService(RIntercept hook, String name, Object instance) {
		
		// very sensitive one [https://github.com/gp-b2g/frameworks_base/blob/master/telephony/java/android/telephony/MSimTelephonyManager.java]
		// can get imei, celllocation, getCurrentPhoneType,getNetworkOperatorName,getSimSerialNumber,getVoiceMailNumber
		
		if ("android.telephony.MSimTelephonyManager".equals(instance.getClass().getName())) {
			/*
			Util.log(hook, Log.WARN, "Telephone service=" + Context.TELEPHONY_SERVICE);
			Class<?> clazz = instance.getClass();
			while (clazz != null) {
				Util.log(hook, Log.WARN, "Class " + clazz);
				for (Method method : clazz.getDeclaredMethods())
					Util.log(hook, Log.WARN, "Declared " + method);
				clazz = clazz.getSuperclass();
			}
			*/
		}
		
		if (name.equals(Context.LOCATION_SERVICE)) {
			// Location manager
			if (!mLocationManagerHooked) {
				hookAll(RTSlocationManager.getInstances(instance), mSecret);
				mLocationManagerHooked = true;
			}
		}
		//*
		else if (name.equals(Context.SENSOR_SERVICE)) {
			// Sensor manager
			if (!mSensorManagerHooked) {
				hookAll(RTSsensor.getInstances(instance), mSecret);
				mSensorManagerHooked = true;
			}
		}
		//*/
		else if (name.equals(Context.ACCOUNT_SERVICE)) {
			// Account manager
			if (!mAccountManagerHooked) {
				hookAll(RTSaccountHandler.getInstances(instance), mSecret);
				mAccountManagerHooked = true;
			}
		}else if (name.equals(Context.WIFI_SERVICE)) {
			// WiFi manager
			if (!mWiFiManagerHooked) {
				hookAll(RTSwifi.getInstances(instance), mSecret);
				mWiFiManagerHooked = true;
			}
		}
		else if (name.equals(Context.CONNECTIVITY_SERVICE)) {
			// Connectivity manager
			if (!mConnectivityHooked) {
				hookAll(RTSnetConnection.getInstances(instance), mSecret);
				mConnectivityHooked = true;
			}
		}
	}

	public void abc(){}
	public static void hookAll(List<RIntercept> listHook, String secret) {
		for (RIntercept hook : listHook)
			hook(hook, secret);
	}

	public static void hookAll(List<RIntercept> listHook, ClassLoader classLoader, String secret) {
		for (RIntercept hook : listHook)
			hook(hook, classLoader, secret);
	}

	private static void hook(RIntercept hook, String secret) {
		hook(hook, null, secret);
	}

	private static void hook(final RIntercept hook, ClassLoader classLoader, String secret) {
		// Check SDK version
	//*
		RIntercept md = null;
		String message = null;
		if (hook.getRestrictionName() == null) {
			if (hook.getSdk() == 0)
				message = "No SDK specified for " + hook;
		} 
		/*else {
			md = PrivacyManager.getHook(hook.getRestrictionName(), hook.getSpecifier());
			if (md == null)
				message = "Hook not found " + hook;
			else if (hook.getSdk() != 0)
				message = "SDK not expected for " + hook;
		}*/
		if (message != null) {
			mListHookError.add(message);
			//Util.log(hook, Log.ERROR, message);
		}
//*/
		int sdk = 0;
		if (hook.getRestrictionName() == null)
			sdk = hook.getSdk();
//		else if (md != null)
//			sdk = md.getSdk();

		if (Build.VERSION.SDK_INT < sdk)
			return;

		// Provide secret
		hook.setSecret(secret);

		try {
			// Create hook method
			XC_MethodHook methodHook = new XC_MethodHook() {
				@Override
				protected void beforeHookedMethod(MethodHookParam param) throws Throwable {
					try {
						if (Process.myUid() <= 0)
							return;
						RObject xparam = RObject.fromXposed(param);
						hook.before(xparam);
						if (xparam.hasResult())
							param.setResult(xparam.getResult());
						if (xparam.hasThrowable())
							param.setThrowable(xparam.getThrowable());
						param.setObjectExtra("xextra", xparam.getExtras());
					} catch (Throwable ex) {
						//Util.bug(null, ex);
					}
				}

				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					if (!param.hasThrowable())
						try {
							if (Process.myUid() <= 0)
								return;
							RObject xparam = RObject.fromXposed(param);
							xparam.setExtras(param.getObjectExtra("xextra"));
							hook.after(xparam);
							if (xparam.hasResult())
								param.setResult(xparam.getResult());
							if (xparam.hasThrowable())
								param.setThrowable(xparam.getThrowable());
						} catch (Throwable ex) {
							//Util.bug(null, ex);
						}
				}
			};

			// Find class
			Class<?> hookClass = null;
			try {
				// hookClass = Class.forName(hook.getClassName());
				hookClass = findClass(hook.getClassName(), classLoader);
			} catch (Throwable ex) {
				 message = String.format("Class not found for %s", hook);
				 mListHookError.add(message);
				// Util.log(hook, hook.isOptional() ? Log.WARN : Log.ERROR, message);
			}

			// Get members
			List<Member> listMember = new ArrayList<Member>();
			Class<?> clazz = hookClass;
			while (clazz != null) {
				if (hook.getMethodName() == null) {
					for (Constructor<?> constructor : clazz.getDeclaredConstructors())
						if (Modifier.isPublic(constructor.getModifiers()) ? hook.isVisible() : !hook.isVisible())
							listMember.add(constructor);
					break;
				} else {
					for (Method method : clazz.getDeclaredMethods())
						if (method.getName().equals(hook.getMethodName())
								&& (Modifier.isPublic(method.getModifiers()) ? hook.isVisible() : !hook.isVisible()))
							listMember.add(method);
				}
				clazz = clazz.getSuperclass();
			}

			
			// Hook members
			for (Member member : listMember)
				try {
				    Log.i("BritiHook","Found:--"+member.getClass()+""+member.getName());
				    
					XposedBridge.hookMethod(member, methodHook);
				} catch (Throwable ex) {
					mListHookError.add(ex.toString());
					//Util.bug(hook, ex);
				}

			// Check if members found
			if (listMember.isEmpty() && !hook.getClassName().startsWith("com.google.android.gms")) {
				  message = String.format("Method not found for %s", hook);
				if (!hook.isOptional())
					mListHookError.add(message);
				//Util.log(hook, hook.isOptional() ? Log.WARN : Log.ERROR, message);
			}
		} catch (Throwable ex) {
			mListHookError.add(ex.toString());
			//Util.bug(hook, ex);
		}
	}

	// WORKAROUND: when a native lib is loaded after hooking, the hook is undone

	private static List<XC_MethodHook.Unhook> mUnhookNativeMethod = new ArrayList<XC_MethodHook.Unhook>();

	@SuppressWarnings("unused")
	private static void registerNativeMethod(final RIntercept hook, Method method, XC_MethodHook.Unhook unhook) {
		if (Process.myUid() > 0) {
			synchronized (mUnhookNativeMethod) {
				mUnhookNativeMethod.add(unhook);
				//Util.log(hook, Log.INFO, "Native " + method + " uid=" + Process.myUid());
			}
		}
	}

	@SuppressWarnings("unused")
	private static void hookCheckNative() {
		try {
			XC_MethodHook hook = new XC_MethodHook() {
				@Override
				protected void afterHookedMethod(MethodHookParam param) throws Throwable {
					if (Process.myUid() > 0)
						try {
							synchronized (mUnhookNativeMethod) {
								//Util.log(null, Log.INFO, "Loading " + param.args[0] + " uid=" + Process.myUid()
									//	+ " count=" + mUnhookNativeMethod.size());
								for (XC_MethodHook.Unhook unhook : mUnhookNativeMethod) {
									XposedBridge.hookMethod(unhook.getHookedMethod(), unhook.getCallback());
									unhook.unhook();
								}
							}
						} catch (Throwable ex) {
							//Util.bug(null, ex);
						}
				}
			};

			Class<?> runtimeClass = Class.forName("java.lang.Runtime");
			for (Method method : runtimeClass.getDeclaredMethods())
				if (method.getName().equals("load") || method.getName().equals("loadLibrary")) {
					XposedBridge.hookMethod(method, hook);
					//Util.log(null, Log.WARN, "Hooked " + method);
				}
		} catch (Throwable ex) {
			//Util.bug(null, ex);
		}
	}
}
