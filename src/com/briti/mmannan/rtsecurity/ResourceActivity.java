package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class ResourceActivity extends ActionBarActivity {

	public final static String EXTRA_MESSAGE1 = "com.briti.mmannan.rtsecurity.MESSAGE";
	List<String> li;
	
	public String[] tableName = {"microphone","microphonetime","camera","cameratime","network","networktime",
            "location","loctime","bluetooth","bluethtime","clipboard","cliptime","sms","smstime", 
            "widget","widgettime","account","acounttime","advertisementid","advtime",
   		 "internet","internettime","phone","phonetime","nfc","nfctime","wifi","wifitime",
   		 "sensor","sensortime","storage","storagetime","ioinfo","iotime","videorecord","videorecordtime","deviceid","deviceidtime"};
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_resource);

		li=new ArrayList<String>();
		 
		 for(int i=0;i<tableName.length;i+=2)
		 {
			 li.add(tableName[i]);
		 }
		 
		 final ListView list=(ListView) findViewById(R.id.listView12);
	        Log.i("BritiList", "I am Here4");
	      
	        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
	        Log.i("BritiList", "I am Here4"+adp.getItem(0));
	        	
	        list.setAdapter(adp);
	        
	        list.setOnItemClickListener(new OnItemClickListener() {
				@Override
                public void onItemClick(AdapterView<?> parent, View view,
                   int position, long id) {
                  
				    // Log.i("BritiDb", "list click");
	                 // ListView Clicked item index
	                 int itemPosition     = position;
	                 
	                 
	        
	                 // ListView Clicked item value
	                  String  itemValue    = (String) list.getItemAtPosition(position);
	                  Log.i("BritiDb", "Start Call"+itemValue);
	                  //------------- Call new Intent ---------------
	                  Intent intent = new Intent(ResourceActivity.this, DisplayAppList.class);
	                  intent.putExtra(EXTRA_MESSAGE1, itemValue);
	                  startActivity(intent);
				}
	        });
	}
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.resource, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}


	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_resource,
					container, false);
			return rootView;
		}
	}
*/
}
