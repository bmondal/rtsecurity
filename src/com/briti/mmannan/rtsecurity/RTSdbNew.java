package com.briti.mmannan.rtsecurity;

import java.io.File;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.nio.channels.FileChannel;

//import biz.bokhorst.xprivacy.Util;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Environment;
import android.os.Process;
import android.util.Log;


public class RTSdbNew {
	SQLiteDatabase mydb;
	 private static String DBNAME = "PERSONS.db";    // THIS IS THE SQLITE <span id="IL_AD3" class="IL_AD">DATABASE FILE</span> NAME.
	 private static String TABLE = "MY_TABLE";

	 public  void setPermissions(String path, int mode, int uid, int gid) {
			try {
				// frameworks/base/core/java/android/os/FileUtils.java
				Class<?> fileUtils = Class.forName("android.os.FileUtils");
				Method setPermissions = fileUtils
						.getMethod("setPermissions", String.class, int.class, int.class, int.class);
				setPermissions.invoke(null, path, mode, uid, gid);
				Log.i("BritiDB", "Changed permission path=" + path + " mode=" + Integer.toOctalString(mode)
						+ " uid=" + uid + " gid=" + gid);
			} catch (Throwable ex) {
				//Util.bug(null, ex);
			}
		}
	private File getDbFile() {
		
		//File data = Environment.getDataDirectory();
		
		//return  new File("/data/data" + File.separator + "RTSdb" + File.separator + DBNAME);
		//return new File("/data/data/com.briti.mmannan.rtsecurity" + File.separator + "database" + File.separator + DBNAME);
		//return new File(Environment.getDataDirectory() + File.separator + "RTSecurity" + File.separator + DBNAME);
		return new File("/db" + File.separator + DBNAME);
		
	}
	public void createTable(){
        try{
        File dbFile = getDbFile();
        Log.i("BritiDB","PATH"+dbFile.getAbsoluteFile());
     // Create database folder
        
        dbFile.getParentFile().mkdirs();

		// Check database folder
		if (dbFile.getParentFile().isDirectory())
			Log.i("BritiDB", "Database folder=" + dbFile.getParentFile());
		else
			Log.i("BritiDB", "Does not exist folder=" + dbFile.getParentFile());
		
		//setPermissions(dbFile.getAbsolutePath(), 0777, Process.SYSTEM_UID, Process.SYSTEM_UID);
/*
		// Move database from data/xprivacy folder
		File folder = new File(Environment.getDataDirectory() + File.separator + "RTSecurity");
		if (folder.exists()) {
			File[] oldFiles = folder.listFiles();
			if (oldFiles != null)
				for (File file : oldFiles)
					if (file.getName().startsWith(DBNAME) || file.getName().startsWith("usage.db")) {
						File target = new File(dbFile.getParentFile() + File.separator + file.getName());
						copy(file, target);
						//Util.log(null, Log.WARN, "Moved " + file + " to " + target + " ok=" + status);
					}
			folder.delete();
		}
*/
        mydb = SQLiteDatabase.openOrCreateDatabase(dbFile, null);
       // mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, appname TEXT, appid INTEGER, microphone TEXT, location TEXT, camera TEXT, sms TEXT, contact TEXT, network TEXT);");
        mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, NAME TEXT, PLACE TEXT);");
        mydb.close();
        
        //setPermissions(dbFile.getAbsolutePath(), 0770, Process.SYSTEM_UID, Process.SYSTEM_UID);
        }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error in creating table", Toast.LENGTH_LONG);
           Log.i("BritiDB", "Table Creation Failed");
        }
        
        
    }

 public void insertIntoTable(){
        try{
        	File dbFile = getDbFile();
        	//setPermissions(dbFile.getAbsolutePath(), 0777, Process.SYSTEM_UID, Process.SYSTEM_UID);
        	mydb = SQLiteDatabase.openOrCreateDatabase(dbFile,null);
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('CODERZHEAVEN','GREAT INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('ANTHONY','USA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SHUING','JAPAN')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('JAMES','INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SOORYA','INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('MALIK','INDIA')");
            mydb.close();
        }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error in inserting into table", Toast.LENGTH_LONG);
        	Log.i("BritiDB", "Table Insertion Failed");
        }
    }
 
 public void insertIntoTable(String msg1,String msg2){
     try{
     	File dbFile = getDbFile();
     	//setPermissions(dbFile.getAbsolutePath(), 0777, Process.SYSTEM_UID, Process.SYSTEM_UID);
         mydb = SQLiteDatabase.openOrCreateDatabase(dbFile,null);
         mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SOORYA','Audio Call')");
         //String dbStr = "(NAME,PLACE) VALUES ('"+msg1+"','"+msg2+"')";
        //mydb.execSQL("INSERT INTO " + TABLE + dbStr);
         mydb.close();
     }catch(Exception e){
         //Toast.makeText(getApplicationContext(), "Error in inserting into table", Toast.LENGTH_LONG);
     	Log.i("BritiDB", "Table Insertion Failed");
     }
 }
 
 public void showTableValues(){
        try{
        	File dbFile = getDbFile();
            mydb = SQLiteDatabase.openOrCreateDatabase(dbFile,null);
            Cursor allrows  = mydb.rawQuery("SELECT * FROM "+  TABLE, null);
            System.out.println("COUNT : " + allrows.getCount());
            Integer cindex = allrows.getColumnIndex("NAME");
            Integer cindex1 = allrows.getColumnIndex("PLACE");
            if(allrows.moveToFirst()){
                do{
                    
                    String ID = allrows.getString(0);
                    String NAME= allrows.getString(1);
                    String PLACE= allrows.getString(2);
                    Log.i("BritiDB", "ID:"+ID+"NAME:"+NAME+"PLACE"+PLACE);
                   // System.out.println("NAME " + allrows.getString(cindex) + " PLACE : "+ allrows.getString(cindex1));
                    //System.out.println("ID : "+ ID  + " || NAME " + NAME + "|| PLACE : "+ PLACE);
 
                   
                }
                while(allrows.moveToNext());
            }
            mydb.close();
         }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error encountered.", Toast.LENGTH_LONG);
        }
    }
 
 public  void copy(File src, File dst) throws IOException {
		FileInputStream inStream = null;
		try {
			inStream = new FileInputStream(src);
			FileOutputStream outStream = null;
			try {
				outStream = new FileOutputStream(dst);
				FileChannel inChannel = inStream.getChannel();
				FileChannel outChannel = outStream.getChannel();
				inChannel.transferTo(0, inChannel.size(), outChannel);
			} finally {
				if (outStream != null)
					outStream.close();
			}
		} finally {
			if (inStream != null)
				inStream.close();
		}
	}

}
