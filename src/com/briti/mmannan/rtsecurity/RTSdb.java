package com.briti.mmannan.rtsecurity;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;

class AppActivity {
	 
    private int id;
    private int mAppid;
    private String mMsg;
    private String tableHeader;
 
    public AppActivity(){}
 
    public AppActivity(int appid, String msg,String dbCoulmName) {
        super();
        this.mAppid = appid;
        this.mMsg = msg;
        this.tableHeader = dbCoulmName;
    }
 
    //getters & setters
 
    @Override
    public String toString() {
        return "Info [id=" + id + ", UID=" + this.mAppid + ", Message=" + this.mMsg
                + "]";
    }
    
    public int getAppid() {return this.mAppid;}
    public String getMsg() {return this.mMsg;}
    public String getTableHeader() {return this.tableHeader;}
    public int getId(){return this.id;}
    
    public void setId(int id) { this.id = id;}
    public void setAppid(int appid) { this.mAppid = appid;}
    public void setMsg(String msg) { this.mMsg = msg;}
    public void setTableHeader(String dbCoulmName) { this.tableHeader = dbCoulmName;}
    
}

public class RTSdb extends SQLiteOpenHelper {

	 SQLiteDatabase mydb;
	 
	 //Map appLastFunid = new HashMap();
    
	// private static String DBNAME = "RTS.db";    // THIS IS THE SQLITE <span id="IL_AD3" class="IL_AD">DATABASE FILE</span> NAME.
	       // THIS IS <span id="IL_AD6" class="IL_AD">THE TABLE</span> NAME
     private Context appContext;
     public enum resourceID { microphone ,camera, network,location,bluetooth,clipboard,sms,widget,account,advertisementid,internet,phone,nfc,wifi,
sensor,storage,ioinfo,videorecord,deviceid}
  // Database Version
     private static final int DATABASE_VERSION = 1;
     // Database Name
     private static final String DATABASE_NAME = "RTS.db";
     private static String TABLE_NAME = "APP_INFO";
     private static String TABLE_MATRIX = "MATRIX_INFO";
     private static String TABLE_MATRIX_BASE = "BASE_MATRIX_INFO";
     // Books table name
     public String[] tableName = {"id","appid","microphone","microphonetime","camera","cameratime","network","networktime",
             "location","loctime","bluetooth","bluethtime","clipboard","cliptime","sms","smstime", 
             "widget","widgettime","account","acounttime","advertisementid","advtime",
    		 "internet","internettime","phone","phonetime","nfc","nfctime","wifi","wifitime",
    		 "sensor","sensortime","storage","storagetime","ioinfo","iotime","videorecord","videorecordtime","deviceid","deviceidtime",
    		 "firstruntime","lastruntime","reffval","rootfun","appname","appversion"};
     
      public RTSdb(Context context) {
         super(context, DATABASE_NAME, null, DATABASE_VERSION);  
     }
     
     @Override
     public void onCreate(SQLiteDatabase db) {
         // SQL statement to create book table
         String CREATE_BOOK_TABLE = "CREATE TABLE IF NOT EXISTS APP_INFO ( " +
                 "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "appid INTEGER, "+ 
        		 "microphone TEXT, "+"microphonetime TEXT,"+
                 "camera TEXT, "+"cameratime TEXT,"+
        		 "network TEXT, "+"networktime TEXT,"+
                 "location TEXT, "+"loctime TEXT, "+
        		 "bluetooth TEXT, "+"bluethtime TEXT,"+
                 "clipboard TEXT, "+"cliptime TEXT,"+ 
        		 "sms TEXT, "+"smstime TEXT,"+ 
                 "widget TEXT, "+"widgettime TEXT,"+
        		 "account TEXT, "+"acounttime TEXT, "+
                 "advertisementid TEXT, "+"advtime TEXT,"+
        		 "internet TEXT , "+"internettime TEXT, "+
        		 "phone TEXT , "+"phonetime TEXT, "+
        		 "nfc TEXT , "+"nfctime TEXT, "+
        		 "wifi TEXT , "+"wifitime TEXT, "+
        		 "sensor TEXT , "+"sensortime TEXT, "+
        		 "storage TEXT , "+"storagetime TEXT, "+
        		 "ioinfo TEXT , "+"iotime TEXT, "+
                 "videorecord TEXT , "+"videorecordtime TEXT, "+
                 "deviceid TEXT , "+"deviceidtime TEXT, "+
                 "firstruntime TEXT , "+"lastruntime TEXT, "+
                 "reffval INTEGER , "+"rootfun INTEGER, "+
                 "appname TEXT , "+"appversion TEXT)";
         
  //   "deviceid TEXT , "+"deviceidtime TEXT, " +
         // create books table
       //  db.execSQL(CREATE_BOOK_TABLE);
         db.execSQL(CREATE_BOOK_TABLE );
         Log.i("BritiDB", "Table1 creation successed");
         
         String CREATE_BASEMATRIX_TABLE = "CREATE TABLE IF NOT EXISTS BASE_MATRIX_INFO ( " +
                 "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "appid INTEGER, "+ 
        		 "row INTEGER, "+"col INTEGER,"+ "value INTEGER)";
         db.execSQL(CREATE_BASEMATRIX_TABLE );
         Log.i("BritiDB", "Table2 creation successed");
         
         String CREATE_MATRIX_TABLE = "CREATE TABLE IF NOT EXISTS MATRIX_INFO ( " +
                 "id INTEGER PRIMARY KEY AUTOINCREMENT, " + "appid INTEGER, "+ 
        		 "row INTEGER, "+"col INTEGER,"+ "value INTEGER)";
         db.execSQL(CREATE_MATRIX_TABLE );
         Log.i("BritiDB", "Table3 creation successed");
         
         this.mydb = db;
     }
  
    
     @Override
     public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
         // Drop older books table if existed
         db.execSQL("DROP TABLE IF EXISTS "+TABLE_NAME);
  
         // create fresh books table
         this.onCreate(db);
     }
     
     public SQLiteDatabase getRTSDbInstance(){ return this.mydb; }
   //---------------------------------------------------------------------
     
     /**
      * CRUD operations (create "add", read "get", update, delete) book + get all books + delete all books
      */
  
     // Books table name
   //  private static final String TABLE_BOOKS = "books";
  
     // Books Table Columns names
     private static final String KEY_ID = "id";
     private static final String KEY_TITLE = "title";
     private static final String KEY_AUTHOR = "author";
  
     private static final String[] COLUMNS = {KEY_ID,KEY_TITLE,KEY_AUTHOR};
  
     public int getTablePos(String name)
     {
    	 int pos=-1;
    	 for(int i=0;i<tableName.length;i++)
    		 if(tableName[i].equals(name))
    		 {
    			 pos = i;
    			 break;
    		 }
    			
    	 return pos;
     }
     
     private String getCurrentTimeFormat(String timeFormat){
    	  String time = "";
    	  SimpleDateFormat df = new SimpleDateFormat(timeFormat);
    	  Calendar c = Calendar.getInstance();
    	  time = df.format(c.getTime());
    	 
    	  return time;
    	}
     public String checkUidExist(SQLiteDatabase db,int uid,String tableHeader)
     {
    	 /*
    	 Cursor cursor = 
                 db.query(TABLE_NAME, // a. table
                 COLUMNS, // b. column names
                 " id = ?", // c. selections 
                 new String[] { String.valueOf(uid) }, // d. selections args
                 null, // e. group by
                 null, // f. having
                 null, // g. order by
                 null);
             */
    	 String countQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE appid="+uid;
         Cursor cursor = db.rawQuery(countQuery, null);
        
        
         if(cursor.getCount()>0)
         {
        	 int pos = getTablePos(tableHeader);
        	 int posPrevFunLocation = getTablePos("rootfun");
        	 
        	 Log.i("BritiDB","Posss:-"+pos+"--"+posPrevFunLocation);
             if (cursor.moveToFirst()) {
            	 String msg = cursor.getString(pos);
            	 String msg1 = cursor.getString(pos+1);
            	 int msg2 = cursor.getInt(posPrevFunLocation);
            	 int msg3 = cursor.getInt(posPrevFunLocation-1);
            	 String firstTime  = cursor.getString(posPrevFunLocation-3);
            	 cursor.close();
            	 Log.i("BritiDB", msg+"#"+msg1+"#"+msg2+"#"+msg3+"#"+firstTime);
               return (msg+"#"+msg1+"#"+msg2+"#"+msg3+"#"+firstTime);
             }
         }
         else
         {
        	 cursor.close();
        	 Log.i("BritiDB", "NO");
        	 return "NO";
         }
    	 
    	 return "";
     }
     
     public int checkMatrixValExist(SQLiteDatabase db,int uid,int row,int col,int reffVal)
     {
  
    	 String MATRIX;
	     if(reffVal==0) MATRIX = TABLE_MATRIX_BASE;
	     else MATRIX = TABLE_MATRIX;
	     
    	 String countQuery = "SELECT  * FROM " + MATRIX +" WHERE appid="+uid+" AND row="+row+" AND col="+col;
         Cursor cursor = db.rawQuery(countQuery, null);
        
         int value=0;
         if(cursor.getCount()>0)
         {
        	 
             if (cursor.moveToFirst()) {
            	 value = cursor.getInt(4);
            	 cursor.close();
            	 Log.i("BritiDB", "table Val#"+value);
             }
         }

    	 
    	 return value;
     }
     
     public void updateMatrixValue(SQLiteDatabase db,int appid,int row, int col, int reffVal)
     {
    	     String MATRIX;
    	     if(reffVal==0) MATRIX = TABLE_MATRIX_BASE;
    	     else MATRIX = TABLE_MATRIX;
    	     
        	 int val = checkMatrixValExist(db,appid,row,col,reffVal);
        	 
        	 Log.i("BritiMX","Appid:"+appid+" Row:"+row+" clo:"+col+" Value:"+val+ "Reff: "+reffVal + "Marix :"+MATRIX);
	    	
	         if(val==0)
	         {
	        	 ContentValues values = new ContentValues();
		         
		         values.put("appid", appid); // get id 
		         values.put("row", row); // function name
		         values.put("col", col);
		         
	        	 values.put("value", 1);
	        	 db.insert(MATRIX,null,values); 
	         }
	         else
	         {
	        	 ContentValues values = new ContentValues();
	        	 
	        	 Log.i("BritiMX","Content Update");
	        	 val = val+1;
	        	 values.put("value", val);
	        	 db.update(MATRIX,
	        			    values,
	        			    "appid = ? AND row = ? AND col = ?",
	        			    new String[]{String.valueOf(appid), String.valueOf(row),String.valueOf(col)});
	        	 //String countQuery = "UPDATE " + TABLE_MATRIX +" SET value="+val+" WHERE appid="+appid+"AND row="+row+"AND col="+col;
	        	 //db.rawQuery(countQuery, null);
	        	 //db.execSQL(countQuery);
	        	 //Cursor cursor = db.rawQuery(countQuery, null);   
	         }
	         
	         db.close();
	     
    	        
     }
     public void addActivity(AppActivity info){
         Log.i("BritiDB","INFO"+info.toString());
         // 1. get reference to writable DB
         Log.i("BritiDB","GetWritableDB Before");
         try{
        	 SQLiteDatabase db = this.getWritableDatabase();
        	 if(db.isOpen())
        	 {
		         if(this==null)
		        	 Log.i("BritiDB","GetWritableDB failed");
		         
		         Log.i("BritiDB","GetWritableDB after");
		         // 2. create ContentValues to add key "column"/value
		         
		         String chkMsg = checkUidExist(db,info.getAppid(),info.getTableHeader());
		         int pos = getTablePos(info.getTableHeader());
		         
		         
		         String timeClm = tableName[pos+1];
		         String curTime = getCurrentTimeFormat("MM/dd/yyyy HH:mm:ss");
		         if(chkMsg.equals("NO"))
		         {
			         ContentValues values = new ContentValues();
			         Log.i("BritiDB","Content failed");
			         values.put("appid", info.getAppid()); // get id 
			         values.put(info.getTableHeader(), info.getMsg()); // function name
			         values.put(timeClm, curTime);
			         
			         values.put("firstruntime", curTime);
			         values.put("reffval", 0);
			         values.put("rootfun", pos);
			         
			         Log.i("BritiDB","Uid"+info.getAppid());
			         Log.i("BritiDB","Title"+info.getMsg());
			         // 3. insert
			         db.insert(TABLE_NAME, // table
			                 null, //nullColumnHack
			                 values); // key/value -> keys = column names/ values = column values
			        
			         
			         // Enter value in matrix ---
			         //appLastFunid.put(info.getAppid(), pos); // kep the las fun call
			         updateMatrixValue(db,info.getAppid(),0, pos,0);
		         }  
		        	 else
		        	 {
		        		// Log.i("BritiUP", "chkmsg:-"+chkMsg);
		        		 String[] separated = chkMsg.split("#");
		        		 String finalMsg = separated[0] + "-" + info.getMsg();
		        		// Log.i("BritiUP", "fir:-"+finalMsg);
		        		 updateBook(db,info,finalMsg);
		        		 String finalMsg1 = separated[1] + "-" + curTime;
		        		// Log.i("BritiUP", "time:-"+finalMsg1 + "clm :"+timeClm);
		        		 updateTime(db, info, timeClm, finalMsg1);
		        		 		        		 
		        		
		        		 int reffVal = Integer.parseInt(separated[3]);
		        	
		        		 String lastTime = separated[4];
		        		 long diff = getTimeDiff(lastTime,curTime);
		        		 
		        		 togglemodeAndUpdateTime(db,info, diff,curTime,reffVal);
		        		 
			        		 updateCallTime(db, info, curTime,"lastruntime");
			        		 updateLastFunCall(db, info, pos);
			        		 // kep the las fun call
			        		 int appID = info.getAppid();
			        		 //int row;
			        		// if(!appLastFunid.containsKey(appID))
			        			// appLastFunid.put(info.getAppid(), pos); 
			        		 
			        		// int row = (Integer) appLastFunid.get(appID);
			        		 int row = Integer.parseInt(separated[2]);
			        		 Log.i("BritiDB","Found row="+row);
			        		 //appLastFunid.put(info.getAppid(), pos);
					         updateMatrixValue(db,appID,row, pos,reffVal);
		        		 
				         
		        	 }
        	 }
        	 else{
        		 Log.i("BritiDB","open failed");
        	 }
         }catch(SQLiteException e){
             Log.i("BritiDB", e.getMessage());
             //sreturn false;
         }//finally{db.close(); };
         // 4. close
         
     }
  //SELECT LastName,FirstName,Address FROM Persons
    // WHERE Address IS NULL
     
public long getTimeDiff(String lastTime,String curTime)
{
	long diffMinutes=0;
	 SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
	 try {
		 Log.i("BritiMX","DIFF MIN="+lastTime);
		 Date d1 = format.parse(lastTime);
		 Date d2 = format.parse(curTime);
		 long diff = d2.getTime() - d1.getTime();
		 diffMinutes = diff / (60 * 1000) % 60;
		 Log.i("BritiMX","DIFF MIN="+diffMinutes);
		
		 return diffMinutes;
		 
	 } catch (Exception e) {
			e.printStackTrace();
		}
	 return diffMinutes;
   
}

public void sendAnotification(String msg)
{
	
}
public void CompareresultWithBase(SQLiteDatabase db,int appid)
{
	int[][] baseMat = new int[40][40];
	int[][] curMat = new int[40][40];
	
	String countQuery = "SELECT  * FROM " + TABLE_MATRIX_BASE +" WHERE appid="+appid;
    Cursor cursor = db.rawQuery(countQuery, null);
 
    if(cursor.getCount()>0)
    {
    	cursor.moveToFirst();
    	do {
    		baseMat[cursor.getInt(2)][cursor.getInt(3)] = cursor.getInt(4);
        } while (cursor.moveToNext());
    }
    
    String countQuery1 = "SELECT  * FROM " + TABLE_MATRIX +" WHERE appid="+appid;
    Cursor cursor1 = db.rawQuery(countQuery1, null);
 
    if(cursor1.getCount()>0)
    {
    	cursor1.moveToFirst();
    	do {
    		curMat[cursor1.getInt(2)][cursor1.getInt(3)] = cursor1.getInt(4);
        } while (cursor1.moveToNext());
    }
	
    int newCall[][] = new int[100][2];
    int mismatch=0;
    
	for(int i=1;i<40;i++)
		for(int j=0;j<40;j++)
		{
			int baseVal = baseMat[i][j];
			int curVal = curMat[i][j];
			if(baseVal>0) baseVal =1;
			if(curVal>0) curVal = 1;
			if(curVal!=baseVal)
			{
				newCall[mismatch][0]=i;
				newCall[mismatch][1]=j;
				mismatch++;
			}
			
		}
	
	Log.i("BritiMX", "Mismatch Found: "+mismatch);
}
public void togglemodeAndUpdateTime(SQLiteDatabase db,AppActivity info,long diff,String curTime,int reffVal)
{
	if(diff>=1 && reffVal==0)
	{
		updateReffVal(db,info,1,"reffval");
		updateCallTime(db,info, curTime, "firstruntime");
		 Log.i("BritiMX", "Change From Reff mode");
	}
	
	if(diff>1 && reffVal==1)
	{
		CompareresultWithBase(db,info.getAppid());
		updateCallTime(db,info, curTime, "firstruntime");
	}
		
}
 public List<String >getAppList(String clmname){
    	 
	 Log.i("BritiDB", "inside getAppList before qr:-"+clmname);
    	 List<String> msgList = new ArrayList<String>();
    	 SQLiteDatabase db = this.getReadableDatabase();
    	 String countQuery = "SELECT * FROM " + TABLE_NAME +" WHERE "+clmname+" IS NOT NULL";
         Cursor cursor = db.rawQuery(countQuery, null);
        
        Log.i("BritiDB", "inside getAppList after");
         if(cursor.getCount()>0)
         {
        	// int pos = getTablePos(clmname);
        	 if (cursor.moveToFirst()) {
	        	 do{
	        		    Log.i("BritiDB", "UID:"+cursor.getString(1));
		            	 msgList.add(cursor.getString(1)); 
		            
		         }
	        	 while(cursor.moveToNext());
        	 }
        	 
        	 cursor.close();
        	 Log.i("BritiDB", "after getAppList");
             return msgList;
         }
         else
         {
        	 cursor.close();
        	 Log.i("BritiDB", "NO");
        	 return msgList;
         }
    	 
    	 //return "";
    	// return msgList;
     }
     
	 public List<String >getMsgOfColumn(int uid,String clmName,int next){
		 
		 List<String> msgList = new ArrayList<String>();
		 SQLiteDatabase db = this.getReadableDatabase();
		 String countQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE appid="+uid;
	     Cursor cursor = db.rawQuery(countQuery, null);
	    
	    
	     if(cursor.getCount()>0)
	     {
	    	 	int pos = getTablePos(clmName) + next;
	             if (cursor.moveToFirst()) {
	            	 String msg = cursor.getString(pos);
	            	 msgList.add(msg);
	//            	 Log.i("BritiDB", msg);
	             }
	     }
	     
	     return msgList;
	 }
 
     public List<String >getMsgList(int uid){
    	 
    	 List<String> msgList = new ArrayList<String>();
    	 SQLiteDatabase db = this.getReadableDatabase();
    	 String countQuery = "SELECT  * FROM " + TABLE_NAME +" WHERE appid="+uid;
         Cursor cursor = db.rawQuery(countQuery, null);
        
        
         if(cursor.getCount()>0)
         {
        	// do{
	             if (cursor.moveToFirst()) {
//	            	 String msg = cursor.getString(2);
//	            	 msgList.add(msg);
//	            	 Log.i("BritiDB", msg);
	            	
	            	 
	            	 for(int i=2;i<tableName.length;i+=2)
	            	 {
	            		 if(!cursor.isNull(i))
	            		 {
		            		// String msg = cursor.getString(i);
	            			 String msg = tableName[i];
		            		 Log.i("BritiDB", msg);
		            		 msgList.add(msg);
	            		 }
	            	 }
	            	 
	            	 cursor.close();
	            	
	               return msgList;
	             }
	         //}
	         //while(cursor.moveToNext());
         }
         else
         {
        	 cursor.close();
        	 Log.i("BritiDB", "NO");
        	 return msgList;
         }
    	 
    	 //return "";
    	 return msgList;
     }
     public AppActivity getActivity(int id){
  
         // 1. get reference to readable DB
         SQLiteDatabase db = this.getReadableDatabase();
  
         // 2. build query
         Cursor cursor = 
                 db.query(TABLE_NAME, // a. table
                 COLUMNS, // b. column names
                 " id = ?", // c. selections 
                 new String[] { String.valueOf(id) }, // d. selections args
                 null, // e. group by
                 null, // f. having
                 null, // g. order by
                 null); // h. limit
  
         // 3. if we got results get the first one
         if (cursor != null)
             cursor.moveToFirst();
  
         // 4. build book object
         AppActivity info = new AppActivity();
         info.setId(Integer.parseInt(cursor.getString(0)));
         info.setAppid(Integer.parseInt(cursor.getString(1)));
         info.setMsg(cursor.getString(2));
  
         Log.i("BritiDB","INFO("+id+")"+ info.toString());
  
         // 5. return book
         return info;
     }
  
     // Get All Books
     public List<AppActivity> getAllBooks() {
         List<AppActivity> books = new LinkedList<AppActivity>();
  
         // 1. build the query
         String query = "SELECT  * FROM " + TABLE_NAME;
  
         // 2. get reference to writable DB
         SQLiteDatabase db = this.getWritableDatabase();
         Cursor cursor = db.rawQuery(query, null);
  
         // 3. go over each row, build book and add it to list
         AppActivity book = null;
         if (cursor.moveToFirst()) {
             do {
                 book = new AppActivity();
                 book.setId(Integer.parseInt(cursor.getString(0)));
                 book.setAppid(Integer.parseInt(cursor.getString(1)));
                 book.setMsg(cursor.getString(2));
  
                 // Add book to books
                 books.add(book);
             } while (cursor.moveToNext());
         }
  
         Log.i("BritiDB","getAllBooks()"+ books.toString());
  
         // return books
         return books;
     }
  
      // Updating single book
     public int updateBook(SQLiteDatabase db,AppActivity book,String msg) {
  
    	 Log.i("BritiDB", "Value Updated start");
         // 1. get reference to writable DB
        // SQLiteDatabase db = this.getWritableDatabase();
  
         // 2. create ContentValues to add key "column"/value
         ContentValues values = new ContentValues();
        // values.put(app, book.getAppid()); // get title 
         values.put(book.getTableHeader(), msg); // get author
  
         // 3. updating row
         int i = db.update(TABLE_NAME, //table
                 values, // column/value
                 "appid = ?", // selections
                 new String[] { String.valueOf(book.getAppid()) }); //selection args
         Log.i("BritiDB", "Value Updated");
         // 4. close
         //db.close();
  
         return i;
  
     }
     
     public int updateCallTime(SQLiteDatabase db,AppActivity book,String time,String tableCol)
     {
    	 ContentValues values = new ContentValues();
         // values.put(app, book.getAppid()); // get title 
          values.put(tableCol,time);
          int i = db.update(TABLE_NAME, //table
                  values, // column/value
                  "appid = ?", // selections
                  new String[] { String.valueOf(book.getAppid()) });
         
          Log.i("BritiDB", "Value Updated lastruntime");
          return i;
    	
     }
     
     public int updateReffVal(SQLiteDatabase db,AppActivity book,int val,String tableCol)
     {
    	 ContentValues values = new ContentValues();
         // values.put(app, book.getAppid()); // get title 
          values.put(tableCol,val);
          int i = db.update(TABLE_NAME, //table
                  values, // column/value
                  "appid = ?", // selections
                  new String[] { String.valueOf(book.getAppid()) });
         
          Log.i("BritiDB", "Value Updated lastruntime");
          return i;
    	
     }
     public int updateLastFunCall(SQLiteDatabase db,AppActivity book,int funid)
     {
    	 ContentValues values = new ContentValues();
         // values.put(app, book.getAppid()); // get title 
          values.put("rootfun", funid);
          int i = db.update(TABLE_NAME, //table
                  values, // column/value
                  "appid = ?", // selections
                  new String[] { String.valueOf(book.getAppid()) });
         
          Log.i("BritiDB", "Value Updated lastruntime");
          return i;
    	
     }
     public int updateTime(SQLiteDatabase db,AppActivity book,String clmName,String msg) {
    	  
         // 1. get reference to writable DB
        // SQLiteDatabase db = this.getWritableDatabase();
  
         // 2. create ContentValues to add key "column"/value
         ContentValues values = new ContentValues();
        // values.put(app, book.getAppid()); // get title 
         values.put(clmName, msg); // get author
  
         Log.i("BritiBD", "Time Updated before qr:="+clmName+"msg "+msg);
         // 3. updating row
         int i = db.update(TABLE_NAME, //table
                 values, // column/value
                 "appid = ?", // selections
                 new String[] { String.valueOf(book.getAppid()) }); //selection args
         Log.i("BritiBD", "Value Updated");
         // 4. close
        // db.close();
  
         Log.i("BritiBD", "Value Updated");
         return i;
  
     }
  
     // Deleting single book
     public void deleteBook(AppActivity book) {
  
         // 1. get reference to writable DB
         SQLiteDatabase db = this.getWritableDatabase();
  
         // 2. delete
         db.delete(TABLE_NAME,
                 KEY_ID+" = ?",
                 new String[] { String.valueOf(book.getId()) });
  
         // 3. close
         db.close();
  
         Log.d("deleteBook", book.toString());
  
     }
 
	 /*
	 public void createTable(){
	        try{
	        mydb = openOrCreateDatabase(DBNAME, appContext.MODE_PRIVATE,null);
	       // mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, appname TEXT, appid INTEGER, microphone TEXT, location TEXT, camera TEXT, sms TEXT, contact TEXT, network TEXT);");
	        mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, NAME TEXT, PLACE TEXT);");
	        mydb.close();
	        }catch(Exception e){
	            //Toast.makeText(getApplicationContext(), "Error in creating table", Toast.LENGTH_LONG);
	           Log.i("BritiDB", "Table Creation Failed");
	        }
	    }
	 public void insertIntoTable(){
	        try{
	            mydb = openOrCreateDatabase(DBNAME, appContext.MODE_PRIVATE,null);
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('CODERZHEAVEN','GREAT INDIA')");
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('ANTHONY','USA')");
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SHUING','JAPAN')");
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('JAMES','INDIA')");
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SOORYA','INDIA')");
	            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('MALIK','INDIA')");
	            mydb.close();
	        }catch(Exception e){
	            //Toast.makeText(getApplicationContext(), "Error in inserting into table", Toast.LENGTH_LONG);
	        	Log.i("BritiDB", "Table Insertion Failed");
	        }
	    }
	 
	 public void showTableValues(){
	        try{
	            mydb = openOrCreateDatabase(DBNAME, appContext.MODE_PRIVATE,null);
	            Cursor allrows  = mydb.rawQuery("SELECT * FROM "+  TABLE, null);
	            System.out.println("COUNT : " + allrows.getCount());
	            Integer cindex = allrows.getColumnIndex("NAME");
	            Integer cindex1 = allrows.getColumnIndex("PLACE");
	 
	           
	 
	            if(allrows.moveToFirst()){
	                do{
	                    
	                    String ID = allrows.getString(0);
	                    String NAME= allrows.getString(1);
	                    String PLACE= allrows.getString(2);
	                    Log.i("BritiDB", "ID:"+ID+"NAME:"+NAME+"PLACE"+PLACE);
	                   // System.out.println("NAME " + allrows.getString(cindex) + " PLACE : "+ allrows.getString(cindex1));
	                    //System.out.println("ID : "+ ID  + " || NAME " + NAME + "|| PLACE : "+ PLACE);
	 
	                   
	                }
	                while(allrows.moveToNext());
	            }
	            mydb.close();
	         }catch(Exception e){
	            //Toast.makeText(getApplicationContext(), "Error encountered.", Toast.LENGTH_LONG);
	        }
	    }
	    */
}
