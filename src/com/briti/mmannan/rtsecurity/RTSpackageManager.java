package com.briti.mmannan.rtsecurity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.bool;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class RTSpackageManager {
	
	private static RTSpackageManager   _instance;
	private  Map<String, String> mAppNameUidMap = new HashMap<String, String>();
	private RTSpackageManager()
	{

	}

	public static RTSpackageManager getInstance()
	{
	    if (_instance == null)
	    {
	        _instance = new RTSpackageManager();
	    }
	    return _instance;
	}
	
	public boolean chekcSysID(String uid)
	{
		if(mAppNameUidMap.isEmpty())
			createAppUidMap();
		
		if( mAppNameUidMap.containsKey(uid))
		{
			Log.i("BTSYS","not system id:-"+uid);
			return false;
		}
		else
		{
			Log.i("BTSYS","system id:-"+uid);
			return true;
		}
	}
	public String versionNo(String uid)
	{
		String version_no=null;
		//PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		//version = pInfo.versionName;
		return version_no;
	}
	
	public void createAppUidMap()
	{
		Log.i("BTSYS","call map:-");
		try {
			Context ctx = RTSappContext.getInstance().getContext();
			   String listStr;
	           List<PackageInfo> appListInfo = ctx.getPackageManager().getInstalledPackages(0);
	           PackageManager pm = ctx.getPackageManager();
	            for (PackageInfo p : appListInfo) {
	                if (!isSystemPackage(p)) {
	                	listStr=p.applicationInfo.loadLabel(pm).toString();
	                	//listStr.format("%d-%s",p.applicationInfo.uid,p.applicationInfo.loadLabel(pm).toString());
	                	//li.add(listStr);
	                	
	                	Log.i("BritiList",""+listStr+"ID:"+p.applicationInfo.uid);
	                	Log.i("BritiPkg",""+listStr+"ID:"+p.applicationInfo.packageName);
	                	mAppNameUidMap.put(p.applicationInfo.uid+"",listStr);
	                	//Log.i("BritiList", "AppUID:-"+p.applicationInfo.uid+"Name:-"+p.applicationInfo.packageName+"--"+p.applicationInfo.loadLabel(pm));
	                }
	            }
	        }catch(Throwable ex){ }
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
	    return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
	            : false;
	}

}
