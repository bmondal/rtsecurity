package com.briti.mmannan.rtsecurity;


import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import android.location.Location;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;
import android.location.GpsSatellite;
import android.location.LocationListener;
import android.location.GpsStatus;

public class RTSlocationManager extends RIntercept {
	private Methods mMethod;
	private String mClassName;
	//private static final Map<LocationListener, XLocationListener> mListener = new WeakHashMap<LocationListener, XLocationListener>();

	private RTSlocationManager(Methods method, String restrictionName, String className) {
		super(restrictionName, method.name(), null);
		mMethod = method;
		mClassName = className;
	}

	private RTSlocationManager(Methods method, String restrictionName, String className, int sdk) {
		super(restrictionName, method.name(), null, sdk);
		mMethod = method;
		mClassName = className;
	}

	public String getClassName() {
		return mClassName;
	}


	// @formatter:on

	// @formatter:off
	private enum Methods {
		addGeofence, 
		getGpsStatus,
		getLastKnownLocation,
		getProviders, isProviderEnabled,
		removeUpdates,
		requestLocationUpdates
	};
	// @formatter:on

	public static List<RIntercept> getInstances(Object instance) {
		String className = instance.getClass().getName();
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		for (Methods loc : Methods.values())
			listHook.add(new RTSlocationManager(loc, "location", className));
//			if (loc == Methods.removeUpdates)
//				listHook.add(new RTSlocationManager(loc, null, className, 3));
//			else
				
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
	//Sets alerts to be notified when the device enters or exits one of the specified geofences. 
		int uid = Binder.getCallingUid();
		int pid = Binder.getCallingUid();
		
		if (mMethod == Methods.addGeofence) {
			Log.i("BritiLoc","caller addGeofence");
			insertIntoDB(uid,"addGeofence","location");

		} else if (mMethod == Methods.removeUpdates) {
			Log.i("BritiLoc","caller removeUpdates");
			insertIntoDB(uid,"removeUpdates","location");

		} else if (mMethod == Methods.requestLocationUpdates) {
			Log.i("BritiLoc","caller requestLocationUpdates");
			insertIntoDB(uid,"requestLocationUpdates","location");

		} else if (mMethod == Methods.getLastKnownLocation) {
			Log.i("BritiLoc","caller getLastKnownLocation----------------got call");
			insertIntoDB(uid,"getLastKnownLocation","location");

		} else if (mMethod == Methods.getProviders) {
			Log.i("BritiLoc","caller locationmng---------getProviders");
			insertIntoDB(uid,"getProviders","location");

		}
	}

	@Override
	protected void after(RObject param) throws Throwable {
					
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) throws InterruptedException {
			
			String sndMsg = uid+"-"+method+"-"+rsc;
			RTSClient cl = new RTSClient(sndMsg);
			cl.sendMessage();
	}

//	private void replaceLocationListener(RObject param, int arg) throws Throwable {
//		
//		
//	}
//
//	private void removeLocationListener(RObject param) {
//		
//	}

//	private class XLocationListener implements LocationListener {
//		private LocationListener mLocationListener;
//
//		public XLocationListener(LocationListener locationListener) {
//			mLocationListener = locationListener;
//		}
//
//		@Override
//		public void onLocationChanged(Location location) {
//			
//		}
//
//		@Override
//		public void onProviderDisabled(String provider) {
//			
//		}
//
//		@Override
//		public void onProviderEnabled(String provider) {
//			mLocationListener.onProviderEnabled(provider);
//		}
//
//		@Override
//		public void onStatusChanged(String provider, int status, Bundle extras) {
//			mLocationListener.onStatusChanged(provider, status, extras);
//		}
//	}
}
