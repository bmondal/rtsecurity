package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import android.bluetooth.BluetoothDevice;
import android.drm.DrmStore.RightsStatus;
import android.os.Binder;
import android.util.Log;

public class RTSbluetooth extends RIntercept {
	private Methods mMethod;

	private RTSbluetooth(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.bluetooth.BluetoothAdapter";
	}

	// public String getAddress()
	// public Set<BluetoothDevice> getBondedDevices()
	// frameworks/base/core/java/android/bluetooth/BluetoothAdapter.java
	// http://developer.android.com/reference/android/bluetooth/BluetoothAdapter.html

	private enum Methods {
		getAddress, getBondedDevices
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSbluetooth(Methods.getAddress, "bluetooth"));
		listHook.add(new RTSbluetooth(Methods.getBondedDevices,"bluetooth"));
		return listHook;
	}

	@Override
	protected void before(RObject obj) throws Throwable {
		// Do nothing
		int uid = Binder.getCallingUid();
		
		if (mMethod == Methods.getAddress) {
			insertIntoDB(uid,"getAddress","bluetooth");
		}else if (mMethod == Methods.getBondedDevices) {
			insertIntoDB(uid,"getBondedDevices","bluetooth");
		}
	}

	@Override
	protected void after(RObject obj) throws Throwable {
		
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
