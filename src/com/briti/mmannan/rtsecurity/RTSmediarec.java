package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.util.Log;

public class RTSmediarec extends RIntercept {
	private Methods mMethod;

	private RTSmediarec(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.media.MediaRecorder";
	}

	// public void setOutputFile(FileDescriptor fd)
	// public void setOutputFile(String path)
	// frameworks/base/media/java/android/media/MediaRecorder.java
	// http://developer.android.com/reference/android/media/MediaRecorder.html

	private enum Methods {
		setOutputFile
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSmediarec(Methods.setOutputFile, "media"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.setOutputFile) {
			int uid = Binder.getCallingUid();
			int pid = Binder.getCallingUid();
			Log.i("BritiHook", "setOutputFile  method Found:-"+uid+" pid:-"+pid);
			insertIntoDB(uid, "startRecording", "camera");

		} else{}
			//Util.log(this, Log.WARN, "Unknown method=" + param.method.getName());
	}

	@Override
	protected void after(RObject param) throws Throwable {
		// Do nothing
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
				
			String sndMsg = uid+"-"+method+"-"+rsc;
			RTSClient cl = new RTSClient(sndMsg);
			cl.sendMessage();
	}

}

