package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class ApplicationActivity extends ActionBarActivity {

	public final static String EXTRA_MESSAGE = "com.briti.mmannan.rtsecurity.MESSAGE";
	List<String> li;
	private Map<String, Integer> mAppNameUidMap = new HashMap<String, Integer>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_application);
	
		createApplist();
		
		final ListView list=(ListView) findViewById(R.id.listView13);
        Log.i("BritiList", "I am Here4");
      
        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
        Log.i("BritiList", "I am Here4"+adp.getItem(0));
        	
        list.setAdapter(adp);
        
        list.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
              
			     Log.i("BritiDb", "list click");
                 // ListView Clicked item index
                 int itemPosition     = position;
                 // ListView Clicked item value
                  String  itemValue    = (String) list.getItemAtPosition(position);
                  Log.i("BritiDb", "Start Call"+itemValue);
                  int uid = mAppNameUidMap.get(itemValue);
                  //------------- Call new Intent ---------------
                  
                  Intent intent = new Intent(ApplicationActivity.this, DisplayResourceList.class);
                  intent.putExtra(EXTRA_MESSAGE, uid+"");
                  startActivity(intent);
                  
			}
        });
	}
	
	protected void createApplist() {
		 li=new ArrayList<String>();
		 
		 try {
			   String listStr;
	           List<PackageInfo> appListInfo = this.getPackageManager().getInstalledPackages(0);
	           PackageManager pm = this.getPackageManager();
	            for (PackageInfo p : appListInfo) {
	                if (!isSystemPackage(p)) {
	                	listStr=p.applicationInfo.loadLabel(pm).toString();
	                	//listStr.format("%d-%s",p.applicationInfo.uid,p.applicationInfo.loadLabel(pm).toString());
	                	if(!listStr.equals("RTSecurity") && !listStr.equals("Xposed Installer"))
	                	{
		                	li.add(listStr);
		                	
		                	Log.i("BritiList",""+listStr);
		                	mAppNameUidMap.put(listStr, p.applicationInfo.uid);
		                	//Log.i("BritiList", "AppUID:-"+p.applicationInfo.uid+"Name:-"+p.applicationInfo.packageName+"--"+p.applicationInfo.loadLabel(pm));
		
	                	}
	                }
	            }
	        }catch(Throwable ex){ }
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
	    return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
	            : false;
	}

}
