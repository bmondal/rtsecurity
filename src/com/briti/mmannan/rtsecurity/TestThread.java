package com.briti.mmannan.rtsecurity;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import android.text.format.Formatter;
import android.util.Log;

public class TestThread {

	public TestThread()
	{	
		new Thread(new Task()).start();
	}
	@SuppressWarnings("deprecation")
	
	class Task implements Runnable {
		   
		    private  ServerSocket serverSocket;
		    private  Socket clientSocket;
		    private  InputStreamReader inputStreamReader;
		    private  BufferedReader bufferedReader;
		    private  String message;
		    
		@Override
		public void run() {
			try {
	            serverSocket = new ServerSocket(4440);  //Server socket
	            Log.i("BritiSRV", "Running server...");
	 
	        } catch (IOException e) {
	            System.out.println("Could not listen on port: 4440");
	        }
	 
	        System.out.println("Server started. Listening to the port 4444");
	 
	        while (true) {
	            try {
	 
	                clientSocket = serverSocket.accept();   //accept the client connection
	                inputStreamReader = new InputStreamReader(clientSocket.getInputStream());
	                bufferedReader = new BufferedReader(inputStreamReader); //get the client message
	                message = bufferedReader.readLine();
	 
	                Log.i("BritiSRV",message);
	                String[] separated = message.split("-");
	                
	               // System.out.println(message);
	                inputStreamReader.close();
	                clientSocket.close();
	                
	                TestDB td = new TestDB(Integer.parseInt(separated[0]), separated[1],separated[2]);
	                
	 
	            } catch (IOException ex) {
	                System.out.println("Problem in message reading");
	            }
	        }
		}
	}
	
	/*
	class Task implements Runnable {
		TestDB db = new TestDB();
		ParseMessage mParser = ParseMessage.getInstance();
		List<String> msgList = new ArrayList<String>();
		@Override
		public void run() {
			while (true) {
				//final int value = i;
				try {
					Thread.sleep(4000);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				
				String msg = ParseMessage.getInstance().GetTopMessage();
				
				if(msg.length()>0)
				{
					Log.i("BritiTHRD", "msg found");

					db.insertIntoDB("thread", msg);
				}
				else
					Log.i("BritiTHRD", "msg Empty");

				//mParser.insertIntoMsgList("Hi test");
					
				/*
				msgList = ParseMessage.getInstance().getListValueWithClearList();
				if(!msgList.isEmpty())
				{
					Log.i("BritiTHRD", "msgList not Empty");
					for(int i=0;i<msgList.size();i++)
						db.insertIntoDB("thread", msgList.get(i));
					msgList.clear();
				}
				else
				{
				   Log.i("BritiTHRD", "msgList Empty");
				}
				//
				//bar.setProgress(value);

			}
		}

	}*/
}
