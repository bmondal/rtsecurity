package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.net.NetworkInfo;
import android.os.Binder;
import android.util.Log;

public class RTSnetConnection extends RIntercept {
	private Methods mMethod;
	private String mClassName;

	private RTSnetConnection(Methods method, String restrictionName, String className) {
		super(restrictionName, method.name(), null);
		mMethod = method;
		mClassName = className;
	}

	public String getClassName() {
		return mClassName;
	}

	// public NetworkInfo getActiveNetworkInfo()
	// public NetworkInfo[] getAllNetworkInfo()
	// public NetworkInfo getNetworkInfo(int networkType)
	// frameworks/base/core/java/android/net/ConnectivityManager.java
	// http://developer.android.com/reference/android/net/ConnectivityManager.html

	private enum Methods {
		getActiveNetworkInfo, getAllNetworkInfo, getNetworkInfo
	};

	public static List<RIntercept> getInstances(Object instance) {
		String className = instance.getClass().getName();
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		for (Methods connmgr : Methods.values())
			listHook.add(new RTSnetConnection(connmgr, "internet", className));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		// Do nothing
	}

	@Override
	protected void after(RObject param) throws Throwable {
		if (mMethod == Methods.getActiveNetworkInfo){
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getActiveNetworkInfo","internet");
		}
		else if(mMethod == Methods.getNetworkInfo) {
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getNetworkInfo","internet");

		} else if (mMethod == Methods.getAllNetworkInfo) {
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getAllNetworkInfo","internet");

		} else{}
			
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		/*
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
		*/
	}
}
