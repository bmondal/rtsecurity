package com.briti.mmannan.rtsecurity;

//public class RTSlocation {
//
//}

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

import android.location.Location;
import android.os.Binder;
import android.util.Log;

//import com.google.android.gms.location.LocationListener;

public class RTSlocation extends RIntercept {
	private Methods mMethod;
	private static final Map<RTSlocationListener, LocationListener> mListener = new WeakHashMap<RTSlocationListener, LocationListener>();

	private RTSlocation(Methods method, String restrictionName) {
		super(restrictionName, method.name(), String.format("GMS.%s", method.name()));
		mMethod = method;
	}

	private RTSlocation(Methods method, String restrictionName, int sdk) {
		super(restrictionName, method.name(), String.format("GMS.%s", method.name()), sdk);
		mMethod = method;
	}

	public String getClassName() {
		return "com.google.android.gms.location.LocationClient";
	}

	// @formatter:off

	// void addGeofences(List<Geofence> geofences, PendingIntent pendingIntent, LocationClient.OnAddGeofencesResultListener listener)
	// Location getLastLocation()
	// void removeGeofences(List<String> geofenceRequestIds, LocationClient.OnRemoveGeofencesResultListener listener)
	// void removeGeofences(PendingIntent pendingIntent, LocationClient.OnRemoveGeofencesResultListener listener)
	// void removeLocationUpdates(LocationListener listener)
	// void removeLocationUpdates(PendingIntent callbackIntent)
	// void requestLocationUpdates(LocationRequest request, PendingIntent callbackIntent)
	// void requestLocationUpdates(LocationRequest request, LocationListener listener)
	// void requestLocationUpdates(LocationRequest request, LocationListener listener, Looper looper)
	// https://developer.android.com/reference/com/google/android/gms/location/LocationClient.html

	// @formatter:on

	private enum Methods {
		addGeofences, getLastLocation, removeGeofences, removeLocationUpdates, requestLocationUpdates
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSlocation(Methods.addGeofences, "location").optional());
		listHook.add(new RTSlocation(Methods.getLastLocation, "location").optional());
		listHook.add(new RTSlocation(Methods.removeGeofences, null, 1).optional());
		listHook.add(new RTSlocation(Methods.removeLocationUpdates, null, 1).optional());
		listHook.add(new RTSlocation(Methods.requestLocationUpdates, "location").optional());
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.addGeofences) {
			
			Log.i("BritiLoc","caller location1");
		} else if (mMethod == Methods.removeGeofences) {
			Log.i("BritiLoc","caller location2");

		} else if (mMethod == Methods.getLastLocation) {
			// Do nothing
			Log.i("BritiLoc","caller location3");

		} else if (mMethod == Methods.removeLocationUpdates) {
			Log.i("BritiLoc","caller location4");

		} else if (mMethod == Methods.requestLocationUpdates) {
			Log.i("BritiLoc","caller location5");

		} else{}
			
	}

	@Override
	protected void after(RObject param) throws Throwable {
		if (mMethod == Methods.addGeofences || mMethod == Methods.removeGeofences) {
			// Do nothing

		} else if (mMethod == Methods.getLastLocation) {
			Location location = (Location) param.getResult();
			

		} else if (mMethod == Methods.removeLocationUpdates) {
			// Do nothing

		} else if (mMethod == Methods.requestLocationUpdates) {
			// Do nothing

		} else{}
			
	}

	private void replaceLocationListener(RObject param) throws Throwable {
		
	}

	private void removeLocationListener(RObject param) {
		
	}

	private class LocationListener implements RTSlocationListener {

		private LocationListener mLocationListener;

		public LocationListener(LocationListener locationListener) {
			mLocationListener = locationListener;
		}

		@Override
		public void onLocationChanged(Location location) {
//			if (location != null)
//				location = PrivacyManager.getDefacedLocation(Binder.getCallingUid(), location);
//			mLocationListener.onLocationChanged(location);
		}
	}
}

