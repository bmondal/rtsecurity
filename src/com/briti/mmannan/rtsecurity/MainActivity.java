package com.briti.mmannan.rtsecurity;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Binder;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends ActionBarActivity {
	public final static String EXTRA_MESSAGE = "com.briti.mmannan.rtsecurity.MESSAGE";
	List<String> li;
	SQLiteDatabase mydb;
	private static String DBNAME = "PERSONS.db";    // THIS IS THE SQLITE <span id="IL_AD3" class="IL_AD">DATABASE FILE</span> NAME.
	private static String TABLE = "MY_TABLE";     
	//private Map<String, Integer> mAppNameUidMap = new HashMap<String, Integer>();
	//private Map<String, String> mAppFndMap = new HashMap<String, String>();
	
	public String[] tableName = {"microphone","microphonetime","camera","cameratime","network","networktime",
            "location","loctime","bluetooth","bluethtime","clipboard","cliptime","sms","smstime", 
            "widget","widgettime","account","acounttime","advertisementid","advtime",
   		 "internet","internettime","phone","phonetime","nfc","nfctime","wifi","wifitime",
   		 "sensor","sensortime","storage","storagetime","ioinfo","iotime","videorecord","videorecordtime"};
	
	public String[] optionName = {"App Activities","Resource Activities","Backup Log"};
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
        Log.i("BritiList", "I am Here1");
       // stopService(new Intent(this, RTServices.class));
        //startService(new Intent(this, RTServices.class));
        //--------- Run SingleTone Class ---
        RTSappContext appContext =  RTSappContext.getInstance();
        appContext.setContext(this);
        //-------------------------------
        //---------------- Thread creation --------------------
        //RTSdb tdn = new RTSdb();  
        
          TestThread tst = new TestThread();
      
        //------------------------------------------------
		/*
        if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
        
		Log.i("BritiList", "I am Here2");
		
		 li=new ArrayList<String>();
		 
		 for(int i=0;i<optionName.length;i++)
		 {
			 li.add(optionName[i]);
		 }
		 /*
		 try {
			   String listStr;
	           List<PackageInfo> appListInfo = this.getPackageManager().getInstalledPackages(0);
	           PackageManager pm = this.getPackageManager();
	            for (PackageInfo p : appListInfo) {
	                if (!isSystemPackage(p)) {
	                	listStr=p.applicationInfo.loadLabel(pm).toString();
	                	//listStr.format("%d-%s",p.applicationInfo.uid,p.applicationInfo.loadLabel(pm).toString());
	                	//li.add(listStr);
	                	
	                	Log.i("BritiList",""+listStr);
	                	mAppNameUidMap.put(listStr, p.applicationInfo.uid);
	                	//Log.i("BritiList", "AppUID:-"+p.applicationInfo.uid+"Name:-"+p.applicationInfo.packageName+"--"+p.applicationInfo.loadLabel(pm));
	                }
	            }
	        }catch(Throwable ex){ }
	    
	    */
	       // final Button show=(Button) findViewById(R.id.button1);
	        final ListView list=(ListView) findViewById(R.id.listView1);
	        Log.i("BritiList", "I am Here4");
	      
	        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
	        Log.i("BritiList", "I am Here4"+adp.getItem(0));
	        	
	        list.setAdapter(adp);
	        
			list.setOnItemClickListener(new OnItemClickListener() {
				@Override
                public void onItemClick(AdapterView<?> parent, View view,
                   int position, long id) {
                  
			     Log.i("BritiDb", "list click");
                 // ListView Clicked item index
                 int itemPosition     = position;
                 
                 
        
                 // ListView Clicked item value
                  String  itemValue    = (String) list.getItemAtPosition(position);
                  Log.i("BritiDb", "Start Call"+itemValue);
                  //------------- Call new Intent ---------------
                if(itemPosition==0)
                {
                  Intent intent = new Intent(MainActivity.this, ApplicationActivity.class);
                  intent.putExtra(EXTRA_MESSAGE, itemValue);
                  startActivity(intent);
                }
                
                if(itemPosition==1)
                {
                  Intent intent = new Intent(MainActivity.this, ResourceActivity.class);
                  intent.putExtra(EXTRA_MESSAGE, itemValue);
                  startActivity(intent);
                }
                
                if(itemPosition==2)
                {
                 	// exportDB();
                }
                  //-------------------------------------
         
                  /*
                 // int uid = mAppNameUidMap.get(itemValue);
                  
                 // Log.i("BritiDB", "UID Click"+uid);
                  RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
                 // List<String> msgList = db.getMsgList(uid);
                  
                  List<String> msgList = db.getAppList(itemValue);
                
                  String finalMsg = "";
                // String callingApp = RTSappContext.getInstance().getContext().getPackageManager().getNameForUid(Binder.getCallingUid());
                  for(int i=0;i<msgList.size();i++)
                  {
                	  finalMsg=finalMsg+"|"+msgList.get(i);
                  }
                  
                  //---------------------  Create custome Dialog -------------------------
                  
                  final Dialog dialog = new Dialog(RTSappContext.getInstance().getContext());

  				//tell the Dialog to use the dialog.xml as it's layout description
  				dialog.setContentView(R.layout.dialog);
  				dialog.setTitle(itemValue+" Info");

  				TextView txt = (TextView) dialog.findViewById(R.id.txt);

  				txt.setText(finalMsg);

  				Button dialogButton = (Button) dialog.findViewById(R.id.dialogButton);

  				dialogButton.setOnClickListener(new OnClickListener() {
  					@Override
  					public void onClick(View v) {
  						dialog.dismiss();
  					}
  				});

  				dialog.show();
  				*/
                  //---------------------------------------------------------------------
                 //  tdn.createTable();
                  // tdn.insertIntoTable();
                   
                   //stopService(new Intent(this, RTServices.class));
               //   TestDB dbt= new TestDB("Hi",itemValue);
                //  RTSdb db = new RTSdb(RTSAppContextProvider.getContext());
                 
                  //db.getAllBooks();
                  //db.close();
                  /*  
                  // Show Alert 
                  Toast.makeText(getApplicationContext(),
                    "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                    .show();
                  */
                  
                }
			});
	
	        
	}
/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
*/
	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		//og.i("BritiList", "I am Here2");
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}
*/
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
	    switch(keyCode)
	    {
	        case KeyEvent.KEYCODE_BACK:

	            moveTaskToBack(true);

	            return true;
	    }
	    return false;
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
	    return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
	            : false;
	}
	
	private void exportDB(){
		
		Log.i("BritiBKUP", "FOUND CALL");
		 String SAMPLE_DB_NAME = "RTS.db";
		File sd = Environment.getExternalStorageDirectory();
	      	File data = Environment.getDataDirectory();
	       FileChannel source=null;
	       FileChannel destination=null;
	       String currentDBPath = "/data/data/"+ "com.briti.mmannan.rtsecurity" +"/databases/"+SAMPLE_DB_NAME;
	       String backupDBPath = SAMPLE_DB_NAME;
	       File currentDB = new File(data, currentDBPath);
	       File backupDB = new File(sd, backupDBPath);
	       Log.i("BritiBKUP", "BEFORE TRY");
	       try {
	            source = new FileInputStream(currentDB).getChannel();
	            destination = new FileOutputStream(backupDB).getChannel();
	            destination.transferFrom(source, 0, source.size());
	            source.close();
	            destination.close();
	            Toast.makeText(this, "DB Exported!", Toast.LENGTH_LONG).show();
	        } catch(IOException e) {
	        	Log.i("BritiBKUP", "GOT EXP");
	        	e.printStackTrace();
	        }
	}
	
	/*
	public void createTable(){
        try{
        mydb = openOrCreateDatabase(DBNAME, Context.MODE_PRIVATE,null);
       // mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, appname TEXT, appid INTEGER, microphone TEXT, location TEXT, camera TEXT, sms TEXT, contact TEXT, network TEXT);");
        mydb.execSQL("CREATE TABLE IF  NOT EXISTS "+ TABLE +" (ID INTEGER PRIMARY KEY, NAME TEXT, PLACE TEXT);");
        mydb.close();
        }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error in creating table", Toast.LENGTH_LONG);
           Log.i("BritiDB", "Table Creation Failed");
        }
    }
 public void insertIntoTable(){
        try{
            mydb = openOrCreateDatabase(DBNAME, Context.MODE_PRIVATE,null);
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('CODERZHEAVEN','GREAT INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('ANTHONY','USA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SHUING','JAPAN')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('JAMES','INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('SOORYA','INDIA')");
            mydb.execSQL("INSERT INTO " + TABLE + "(NAME, PLACE) VALUES('MALIK','INDIA')");
            mydb.close();
        }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error in inserting into table", Toast.LENGTH_LONG);
        	Log.i("BritiDB", "Table Insertion Failed");
        }
    }
 
 public void showTableValues(){
        try{
            mydb = openOrCreateDatabase(DBNAME, Context.MODE_PRIVATE,null);
            Cursor allrows  = mydb.rawQuery("SELECT * FROM "+  TABLE, null);
            System.out.println("COUNT : " + allrows.getCount());
            Integer cindex = allrows.getColumnIndex("NAME");
            Integer cindex1 = allrows.getColumnIndex("PLACE");
 
           
 
            if(allrows.moveToFirst()){
                do{
                    
                    String ID = allrows.getString(0);
                    String NAME= allrows.getString(1);
                    String PLACE= allrows.getString(2);
                    Log.i("BritiDB", "ID: "+ID+" NAME: "+NAME+" PLACE "+PLACE);
                   // System.out.println("NAME " + allrows.getString(cindex) + " PLACE : "+ allrows.getString(cindex1));
                    //System.out.println("ID : "+ ID  + " || NAME " + NAME + "|| PLACE : "+ PLACE);
 
                   
                }
                while(allrows.moveToNext());
            }
            mydb.close();
         }catch(Exception e){
            //Toast.makeText(getApplicationContext(), "Error encountered.", Toast.LENGTH_LONG);
        }
    }
 */
}
