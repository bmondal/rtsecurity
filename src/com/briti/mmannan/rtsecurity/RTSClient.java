package com.briti.mmannan.rtsecurity;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Enumeration;

import android.os.Message;
import android.text.format.Formatter;
import android.util.Log;

public class RTSClient {
	private Socket client;
	 private PrintWriter printwriter;
	// private EditText textField;
   //	 private Button button;
	 private String messsage;
	 
	 public RTSClient(String msg)
	 {
		 Log.i("BritiSRV","Client Constrk"+msg);
		 this.messsage = msg;
	 }
	 
	 public String getLocalIpAddress() {
		 String ip = "";
		  try {
		   Enumeration<NetworkInterface> enumNetworkInterfaces = NetworkInterface
		     .getNetworkInterfaces();
		   while (enumNetworkInterfaces.hasMoreElements()) {
		    NetworkInterface networkInterface = enumNetworkInterfaces
		      .nextElement();
		    Enumeration<InetAddress> enumInetAddress = networkInterface
		      .getInetAddresses();
		    while (enumInetAddress.hasMoreElements()) {
		     InetAddress inetAddress = enumInetAddress.nextElement();

		     if (inetAddress.isSiteLocalAddress()) {
		      ip += inetAddress.getHostAddress();
		     }
		     
		    }

		   }

		  } catch (SocketException e) {
		   // TODO Auto-generated catch block
		   e.printStackTrace();
		   ip += "Something Wrong! " + e.toString() + "\n";
		  }

		  return ip;
	 }
	 
	 public void sendMessage()
	 {
		 new Thread(new TaskSenMsg(this.messsage,"127.0.0.1")).start();
		 /*
		 int snd=0;
//		 while(true)
//		 {
//			 if(snd==1) break;
			 try {
				 Log.i("BritiSRV","before srvr cl");
			     client = new Socket("127.0.0.1", 4440);  //connect to server
			     snd=1;
			     Log.i("BritiSRV","After connection");
			     printwriter = new PrintWriter(client.getOutputStream(),true);
			     Log.i("BritiSRV",messsage);
			     printwriter.write(messsage);  //write the message to output stream
			 
			     printwriter.flush();
			     printwriter.close();
			     client.close();   //closing the connection
			 
			    } catch (UnknownHostException e) {
			     Log.i("BREXP","msg:1");
			    } catch (IOException e) {
			     e.printStackTrace();
			     Log.i("BREXP","msg:2");
			    }
		// }
		 
		 */
	 }
	 
		class TaskSenMsg implements Runnable {
			   
			private Socket client;
			 private PrintWriter printwriter;
		     public String finMsg;
		     public String ip;
			public TaskSenMsg(String msg,String mIp)
			{
				this.finMsg = msg;
				this.ip = mIp;
			}
			 
			@Override
			public void run() {
				String messsage = finMsg;//"10082-"+"takePicture-"+"camera";

				int snd=0;
				int count=0;
//				 while(true)
//				 {
//					 if(snd==1 || count==3) break;
					 try {
//						 Log.i("BritiSRV","before slp");
//						 try{
//							 Thread.sleep(1000);
//						 }catch(Exception e){}
//						 count++;
						 Log.i("BritiSRV","before srvr cl");
					    // client = new Socket("127.0.0.1", 4440);  //connect to server
					     client = new Socket(ip, 4440);
					    // snd=1;
					     Log.i("BritiSRV","After connection");
					     printwriter = new PrintWriter(client.getOutputStream(),true);
					     Log.i("BritiSRV",messsage);
					     printwriter.write(messsage);  //write the message to output stream
					 
					     printwriter.flush();
					     printwriter.close();
					     client.close();   //closing the connection
					 
					    } catch (UnknownHostException e) {
					     e.printStackTrace();
					     Log.i("BritiSRV","EXP msg:1");
					    } catch (IOException e) {
					     e.printStackTrace();
					     Log.i("BritiSRV","EXP msg:2");
					    }
				// }
				 
			
			}
		}
}
