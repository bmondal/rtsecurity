package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.os.Build;

public class TimeView extends ActionBarActivity {
	List<String> li;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_time_view);
		Log.i("BRITICHK", "I am in time view");
		Intent intent = getIntent();
		String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
		//sendMessage = message; 
		li=new ArrayList<String>();
		createList(message);
		
		final ListView list=(ListView) findViewById(R.id.listView3);
        Log.i("BritiList", "I am Here4");
      
        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
        Log.i("BRITICHK", "I am Here4"+adp.getItem(0));
        	
        list.setAdapter(adp);
        
		/*
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
	}

	private void createList(String msg)
	{
		String[] separated = msg.split("#");
		
		String[] funName = separated[0].split("-");
		String[] callTime = separated[1].split("-");
		Log.i("BRITICHK", "LNTH:"+funName.length + "LN2"+callTime.length);
		for(int i=0;i<callTime.length;i++)
		{
			String msgStr = callTime[i]+" ("+funName[i]+" )";
			Log.i("BRITICHK", "LP:"+msgStr);
			li.add(msgStr);
		}
		
	}
	/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.time_view, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_time_view,
					container, false);
			return rootView;
		}
	}
*/
}
