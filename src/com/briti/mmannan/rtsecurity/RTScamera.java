package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.util.Log;
import android.widget.Toast;

public class RTScamera extends RIntercept {
	private Methods mMethod;

	private RTScamera(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.hardware.Camera";
	}

	
	private enum Methods {
		setPreviewCallback, setPreviewCallbackWithBuffer, setOneShotPreviewCallback, takePicture
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		for (Methods cam : Methods.values())
			listHook.add(new RTScamera(cam, "media"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.setPreviewCallback || mMethod == Methods.setPreviewCallbackWithBuffer
				|| mMethod == Methods.setOneShotPreviewCallback || mMethod == Methods.takePicture) {
			int uid = Binder.getCallingUid();
			
			String methodStr = "";
			if (mMethod == Methods.setPreviewCallback) methodStr = "setPreviewCallback";
			if (mMethod == Methods.setPreviewCallbackWithBuffer) methodStr = "setPreviewCallbackWithBuffer";
			if (mMethod == Methods.setOneShotPreviewCallback) methodStr = "setOneShotPreviewCallback";
			if (mMethod == Methods.takePicture) methodStr = "takePicture";
			Log.i("BritiHook", "Camera  method Found:-"+uid);
			
			insertIntoDB(uid, methodStr, "camera");
			//if (isRestricted(param))
				//param.setResult(null);

		} else
			Log.i("BritiHook", "Camera hook method not match");
			//Util.log(this, Log.WARN, "Unknown method=" + param.method.getName());
	}

	@Override
	protected void after(RObject param) throws Throwable {
		// Do nothing
	}
	
protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
}
	
//	protected void insertIntoDB(int uid,String method,String rsc) {
//			
//			String sndMsg = uid+"-"+method+"-"+rsc;
//			Log.i("BritiHook", "snd msg: "+sndMsg);
//			RTSClient cl = new RTSClient(sndMsg);
//			cl.sendMessage();
//	}
}

