package com.briti.mmannan.rtsecurity;

import android.app.Application;
import android.content.Context;
import android.util.Log;

public class RTSAppContextProvider extends Application {
	
	 private static Context sContext;
	 
	    @Override
	    public void onCreate() {
	        super.onCreate();
	        
	        Log.i("BritiDB", "Context Created");
	        
	        sContext = getApplicationContext();
	 
	    }
	 
	    /**
	     * Returns the application context
	     *
	     * @return application context
	     */
	    public static Context getContext() {
	        return sContext;
	    }
	 

}
