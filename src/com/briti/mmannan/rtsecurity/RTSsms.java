package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.telephony.SmsMessage;


public class RTSsms extends RIntercept {
	private Methods mMethod;

	private RTSsms(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.telephony.SmsManager";
	}

	private enum Methods {
		getAllMessagesFromIcc, sendDataMessage, sendMultipartTextMessage, sendTextMessage
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSsms(Methods.getAllMessagesFromIcc, "sms"));
		listHook.add(new RTSsms(Methods.sendDataMessage, "sms"));
		listHook.add(new RTSsms(Methods.sendMultipartTextMessage, "sms"));
		listHook.add(new RTSsms(Methods.sendTextMessage, "sms"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		int uid = Binder.getCallingUid();
		if (mMethod == Methods.sendDataMessage)
			insertIntoDB(uid,"sendDataMessage","sms");
		if(mMethod == Methods.sendMultipartTextMessage)
			insertIntoDB(uid,"sendMultipartTextMessage","sms");
		if(mMethod == Methods.sendTextMessage)
			insertIntoDB(uid,"sendTextMessage","sms");

	}

	@Override
	protected void after(RObject param) throws Throwable {
		
		if (mMethod != Methods.sendDataMessage && mMethod != Methods.sendMultipartTextMessage
				&& mMethod != Methods.sendTextMessage)
			if (mMethod == Methods.getAllMessagesFromIcc) {
				if (param.getResult() != null)
				{
					int uid = Binder.getCallingUid();
					insertIntoDB(uid,"readTextMessage","sms");
				}
					
			} 
	}
	
   protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}

