package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.util.Log;
import android.widget.Toast;

public class RTSmicrophone extends RIntercept {
	private Methods mMethod;

	private RTSmicrophone(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.media.AudioRecord";
	}

	private enum Methods {
		startRecording, stop
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSmicrophone(Methods.startRecording, "media"));
		listHook.add(new RTSmicrophone(Methods.stop, "media"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.startRecording) {
			int uid = Binder.getCallingUid();
			Log.i("BritiHook","Called Audio Recording-Uid:-"+uid);
			//ParseMessage.getInstance().insertIntoMsgList("Call Audio Recodring");
			insertIntoDB(uid,"startRecording","microphone");
		
			
		//	Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
			//if (isRestricted(param))
				//param.setResult(null);

		} //else
			//Util.log(this, Log.WARN, "Unknown method=" + param.method.getName());
		if (mMethod == Methods.stop) {
			int uid = Binder.getCallingUid();
			Log.i("BritiHook","Called Audio Recording Stop-Uid:-"+uid);
			//ParseMessage.getInstance().insertIntoMsgList("Call Stop Recodring");
			 insertIntoDB(uid,"stop","microphone");
		}
	}

	@Override
	protected void after(RObject param) throws Throwable {
		// Do nothing
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
		// RTSdbNew tdn = new RTSdbNew();  
		 //tdn.createTable();
	     //tdn.insertIntoTable("10077",msg);
	      //  tdn.insertIntoTable();
	       //tdn.showTableValues();
	       
		//TestDB td = new TestDB("10077",msg);
		
       // td.insertIntoDB("10077", msg);
		/*
		RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
	        
	        
	        // add Books
	        db.addActivity(new AppActivity(uid, msg));  
	        // get all books
	        List<AppActivity> list1 = db.getAllBooks();
	        db.close();
	        */
	}
}
