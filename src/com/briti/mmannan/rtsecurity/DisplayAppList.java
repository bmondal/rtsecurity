package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.TextView;
import android.os.Build;

public class DisplayAppList extends ActionBarActivity {

	protected static final String EXTRA_MESSAGE = "com.briti.mmannan.rtsecurity.MESSAGE";
	
	
	List<String> li;
	String sendMessage="";
	private Map<String, String> mAppNameUidMap = new HashMap<String, String>();
	private Map<String, String> mAppNameUidMapRev = new HashMap<String, String>();
	private Map<String, String> mSpcialAppMap = new HashMap<String, String>();
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_app_list);

		Intent intent = getIntent();
		String message = intent.getStringExtra(ResourceActivity.EXTRA_MESSAGE1);
		sendMessage = message; 
		li=new ArrayList<String>();
		li.add(sendMessage);
		 createAppUidMap();
		 getValueFromDb(message);

		// mAppNameUidMap.get(itemValue);
		 final ListView list=(ListView) findViewById(R.id.listView2);
	        Log.i("BritiList", "I am Here4");
	      
	        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
	        Log.i("BritiList", "I am Here4"+adp.getItem(0));
	        	
	        list.setAdapter(adp);
	        list.setOnItemClickListener(new OnItemClickListener() {
				@Override
                public void onItemClick(AdapterView<?> parent, View view,
                   int position, long id) {
                  
					if(position==0) return;
			     Log.i("BritiDb", "list click");
                 // ListView Clicked item index
                 int itemPosition     = position;
                 // ListView Clicked item value
                  String  itemValue    = (String) list.getItemAtPosition(position);
                  int uid;
                  if(itemValue.contains("com.") || itemValue.contains("android."))
                	  uid = Integer.parseInt(mSpcialAppMap.get(itemValue));
                  else
                	  uid = Integer.parseInt(mAppNameUidMapRev.get(itemValue));
                  Log.i("BritiDb", "Start Call"+itemValue);
                  
                  RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
                  List<String> msgList1 = db.getMsgOfColumn(uid,sendMessage,0);
                  List<String> msgList2 = db.getMsgOfColumn(uid,sendMessage,1);
                  String str = buildString(msgList1,msgList2);
                //------------- Call new Intent ---------------
                  Log.i("BRITICHK", "FinalSr:-"+str);
                  Intent intent = new Intent(DisplayAppList.this, TimeView.class);
                  intent.putExtra(EXTRA_MESSAGE, str);
                  startActivity(intent);

				}
	        });
		/*
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
	}
	
	private String buildString(List<String> str1,List<String> str2)
	{
		String finStr="";
		String strMsg1 = "";
		String strMsg2 = "";
		for(int i=0;i<str1.size();i++)
		{
			strMsg1+=str1.get(i);
			strMsg2+=str2.get(i);
		}
		
		finStr = strMsg1+"#"+strMsg2;
		
		return finStr;
	}
	public void getValueFromDb(String rscName)
	{
		 RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
         // List<String> msgList = db.getMsgList(uid);
          
          List<String> msgList = db.getAppList(rscName);
        
          String finalMsg = "";
        // String callingApp = RTSappContext.getInstance().getContext().getPackageManager().getNameForUid(Binder.getCallingUid());
          for(int i=0;i<msgList.size();i++)
          {
        	  Log.i("APPLST", "App name here "+msgList.get(i));
        	 if( mAppNameUidMap.containsKey(msgList.get(i)))
        	 {
        		 Log.i("APPLST", "App name here"+msgList.get(i));
        		 li.add(mAppNameUidMap.get(msgList.get(i)));
        	 }
        	 else
        	 {
        		
        		 String callingApp = this.getPackageManager().getNameForUid( Integer.parseInt(msgList.get(i)));
        		 li.add(callingApp);
        		 mSpcialAppMap.put(callingApp, msgList.get(i));
        	 }
        		 
        	  //finalMsg=finalMsg+"|"+msgList.get(i);
        	  
          }
	}
	public void createAppUidMap()
	{
		try {
			   String listStr;
	           List<PackageInfo> appListInfo = this.getPackageManager().getInstalledPackages(0);
	           PackageManager pm = this.getPackageManager();
	            for (PackageInfo p : appListInfo) {
	                if (!isSystemPackage(p)) {
	                	listStr=p.applicationInfo.loadLabel(pm).toString();
	                	//listStr.format("%d-%s",p.applicationInfo.uid,p.applicationInfo.loadLabel(pm).toString());
	                	//li.add(listStr);
	                	
	                	Log.i("BritiList",""+listStr+"ID:"+p.applicationInfo.uid);
	                	Log.i("BritiPkg",""+listStr+"ID:"+p.applicationInfo.packageName);
	                	mAppNameUidMap.put(p.applicationInfo.uid+"",listStr);
	                	mAppNameUidMapRev.put(listStr, p.applicationInfo.uid+"");
	                	//Log.i("BritiList", "AppUID:-"+p.applicationInfo.uid+"Name:-"+p.applicationInfo.packageName+"--"+p.applicationInfo.loadLabel(pm));
	                }
	            }
	        }catch(Throwable ex){ }
	}
	
	private boolean isSystemPackage(PackageInfo pkgInfo) {
	    return ((pkgInfo.applicationInfo.flags & ApplicationInfo.FLAG_SYSTEM) != 0) ? true
	            : false;
	}

/*
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display_app_list, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
*/
	/**
	 * A placeholder fragment containing a simple view.
	 */
	/*
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(
					R.layout.fragment_display_app_list, container, false);
			return rootView;
		}
	}
	*/

}
