package com.briti.mmannan.rtsecurity;



import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.os.Environment;
import android.util.Log;



public class RTSstorage extends RIntercept {
	private Methods mMethod;

	private RTSstorage(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.os.Environment";
	}

	private enum Methods {
		getExternalStorageState
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSstorage(Methods.getExternalStorageState, "storage"));
		return listHook;
	}

	@Override
	protected void before(RObject obj) throws Throwable {
		if (mMethod == Methods.getExternalStorageState) {
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getExternalStorageState","storage");
		} 
	}

	@Override
	protected void after(RObject obj) throws Throwable {
		
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
