package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;

public class RTSappActivity extends RIntercept {
	private Methods mMethod;
	private String mActionName;

	private RTSappActivity(Methods method, String restrictionName, String actionName) {
		super(restrictionName, method.name(), actionName);
		mMethod = method;
		mActionName = actionName;
	}

	private RTSappActivity(Methods method, String restrictionName, String actionName, int sdk) {
		super(restrictionName, method.name(), actionName, sdk);
		mMethod = method;
		mActionName = actionName;
	}

	public String getClassName() {
		return "android.app.Activity";
	}

	// @formatter:on

	private enum Methods {
		getSystemService, startActivities, startActivity, startActivityForResult, startActivityFromChild, startActivityFromFragment, startActivityIfNeeded
	};

	@SuppressLint("InlinedApi")
	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSappActivity(Methods.getSystemService, null, null, 1));

		/*
		List<Methods> startMethods = new ArrayList<Methods>(Arrays.asList(Methods.values()));
		startMethods.remove(Methods.getSystemService);

		// Intent send: browser
		for (Methods activity : startMethods)
			listHook.add(new RTSappActivity(activity, "view", Intent.ACTION_VIEW));

		// Intent send: call
		for (Methods activity : startMethods)
			listHook.add(new RTSappActivity(activity,"call", Intent.ACTION_CALL));

		// Intent send: media
		for (Methods activity : startMethods) {
			listHook.add(new RTSappActivity(activity, "media", MediaStore.ACTION_IMAGE_CAPTURE));
			listHook.add(new RTSappActivity(activity, "media", MediaStore.ACTION_IMAGE_CAPTURE_SECURE));
			listHook.add(new RTSappActivity(activity, "media", MediaStore.ACTION_VIDEO_CAPTURE));
		}
 	*/
		return listHook;
	}

	@Override
	@SuppressLint("DefaultLocale")
	protected void before(RObject param) throws Throwable {
		
	}

	@Override
	protected void after(RObject obj) throws Throwable {
		if (mMethod == Methods.getSystemService) {
			if (obj.args.length > 0 && obj.args[0] != null) {
				String name = (String) obj.args[0];
				Object instance = obj.getResult();
				if (name != null && instance != null)
					RTSecurity.handleGetSystemService(this, name, instance);
			}
		}
	}
}

