package com.briti.mmannan.rtsecurity;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

import com.briti.mmannan.rtsecurity.TestThread.Task;

import android.graphics.Camera;
import android.os.Binder;
import android.util.Log;
import android.widget.Toast;

public class RTScameras extends RIntercept {
	private Methods mMethod;

	private RTScameras(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.hardware.Camera";
	}

	private enum Methods {
		takePicture
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTScameras(Methods.takePicture, "media"));
		//listHook.add(new RTScameras(Methods.stop, "media"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.takePicture) {
			int uid = Binder.getCallingUid();
			int pid = Binder.getCallingUid();
			Log.i("BritiHook", "setOutputFile  method Found:-"+uid+" pid:-"+pid);
			insertIntoDB(uid,"takePicture","camera");
			//ParseMessage.getInstance().insertIntoMsgList("Call Audio Recodring")
		
			
		//	Toast.makeText(this, "The new Service was Created", Toast.LENGTH_LONG).show();
			//if (isRestricted(param))
				//param.setResult(null);

		} //else
			//Util.log(this, Log.WARN, "Unknown method=" + param.method.getName());
//		if (mMethod == Methods.stop) {
//			int uid = Binder.getCallingUid();
//			Log.i("BritiHook","Called Audio Recording Stop-Uid:-"+uid);
//			//ParseMessage.getInstance().insertIntoMsgList("Call Stop Recodring");
//			 insertIntoDB(uid,"stop","microphone");
//		}
	}

	@Override
	protected void after(RObject param) throws Throwable {
		// Do nothing
//		try
//		{
//			Thread.sleep(4000);
//		}catch(Exception e){}
//		int uid = Binder.getCallingUid();
//		Log.i("BritiHook","Called camera chkafter-Uid:-"+uid);
//		insertIntoDB(uid,"takePicture","camera");
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) throws InterruptedException {
		
		//new Thread(new TaskSenMsg()).start();
		//*
		Log.i("BritiHook","Break sleep-Uid:-"+uid);
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
		// RTSdbNew tdn = new RTSdbNew();  
		 //tdn.createTable();
	     //tdn.insertIntoTable("10077",msg);
	      //  tdn.insertIntoTable();
	       //tdn.showTableValues();
	       
		//TestDB td = new TestDB("10077",msg);
		
       // td.insertIntoDB("10077", msg);
		/*
		RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
	        
	        
	        // add Books
	        db.addActivity(new AppActivity(uid, msg));  
	        // get all books
	        List<AppActivity> list1 = db.getAllBooks();
	        db.close();
	        */
	}
	/*
	class TaskSenMsg implements Runnable {
		   
		private Socket client;
		 private PrintWriter printwriter;
	    
		@Override
		public void run() {
			String messsage = "10082-"+"takePicture-"+"camera";

			int snd=0;
			 while(true)
			 {
				 if(snd==1) break;
				 try {
					 Log.i("BritiSRV","before slp");
					 try{
						 Thread.sleep(2000);
					 }catch(Exception e){}
					 Log.i("BritiSRV","before srvr cl");
				     client = new Socket("127.0.0.1", 4440);  //connect to server
				     snd=1;
				     Log.i("BritiSRV","After connection");
				     printwriter = new PrintWriter(client.getOutputStream(),true);
				     Log.i("BritiSRV",messsage);
				     printwriter.write(messsage);  //write the message to output stream
				 
				     printwriter.flush();
				     printwriter.close();
				     client.close();   //closing the connection
				 
				    } catch (UnknownHostException e) {
				     e.printStackTrace();
				     Log.i("BritiSRV","EXP msg:1");
				    } catch (IOException e) {
				     e.printStackTrace();
				     Log.i("BritiSRV","EXP msg:2");
				    }
			 }
			 
		
		}
	}
	//*/
}


