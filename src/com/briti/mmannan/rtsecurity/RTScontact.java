package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import android.annotation.SuppressLint;
import android.content.SyncAdapterType;
import android.content.SyncInfo;
import android.database.Cursor;
import android.database.MatrixCursor;
import android.net.Uri;
import android.os.Binder;
import android.os.DeadObjectException;
import android.text.TextUtils;
import android.util.Log;

public class RTScontact extends RIntercept {
	private Methods mMethod;

	private RTScontact(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	private RTScontact(Methods method, String restrictionName, int sdk) {
		super(restrictionName, "query", null, sdk);
		mMethod = method;
	}

	public String getClassName() {
		return (mMethod == Methods.cquery ? "android.content.ContentProviderClient" : "android.content.ContentResolver");
	}

	// @formatter:off

	// public static SyncInfo getCurrentSync()
	// static List<SyncInfo> getCurrentSyncs()
	// static SyncAdapterType[] getSyncAdapterTypes()

	// public Cursor query(Uri url, String[] projection, String selection, String[] selectionArgs, String sortOrder)
	// public Cursor query(Uri url, String[] projection, String selection, String[] selectionArgs, String sortOrder, CancellationSignal cancellationSignal)

	// TODO: public final void registerContentObserver(Uri uri, boolean notifyForDescendents, ContentObserver observer)

	// https://developers.google.com/gmail/android/
	// http://developer.android.com/reference/android/content/ContentResolver.html
	// http://developer.android.com/reference/android/content/ContentProviderClient.html

	// http://developer.android.com/reference/android/provider/Contacts.People.html
	// http://developer.android.com/reference/android/provider/ContactsContract.Contacts.html
	// http://developer.android.com/reference/android/provider/ContactsContract.Data.html
	// http://developer.android.com/reference/android/provider/ContactsContract.PhoneLookup.html
	// http://developer.android.com/reference/android/provider/ContactsContract.Profile.html
	// http://developer.android.com/reference/android/provider/ContactsContract.RawContacts.html

	// frameworks/base/core/java/android/content/ContentResolver.java

	// @formatter:on

	private enum Methods {
		query, cquery
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTScontact(Methods.query, null, 1));
		listHook.add(new RTScontact(Methods.cquery, null, 1));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		if (mMethod == Methods.query || mMethod == Methods.cquery)
		{
			handleUriBefore(param);
		}
	}

	@Override
	protected void after(RObject param) throws Throwable {
		
	}
	
	@SuppressLint("DefaultLocale")
	private void handleUriBefore(RObject param) throws Throwable {
		// Check URI
		if (param.args.length > 1 && param.args[0] instanceof Uri) {
			String uri = ((Uri) param.args[0]).toString().toLowerCase();
			String[] projection = (param.args[1] instanceof String[] ? (String[]) param.args[1] : null);
			//Util.log(this, Log.INFO, "Before uri=" + uri);

			if (uri.startsWith("content://com.android.contacts/contacts/name_phone_or_email")) {
				// Do nothing

			} else if (uri.startsWith("content://com.android.contacts/contacts")
					|| uri.startsWith("content://com.android.contacts/data")
					|| uri.startsWith("content://com.android.contacts/phone_lookup")
					|| uri.startsWith("content://com.android.contacts/raw_contacts")) {
				
					int uid = Binder.getCallingUid();
					insertIntoDB(uid,"contacts","phone");
			}
		}
	}

	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
