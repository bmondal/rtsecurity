package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.List;

import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBar;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.os.Build;

public class DisplayResourceList extends ActionBarActivity {

	protected static final String EXTRA_MESSAGE = "com.briti.mmannan.rtsecurity.MESSAGE";
	List<String> li;
	String sendMessage="";
	int uid;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display_resource_list);
		
		Intent intent = getIntent();
		String message = intent.getStringExtra(ApplicationActivity.EXTRA_MESSAGE);
		sendMessage = message; 
		li=new ArrayList<String>();
		//li.add("Using Res");
		uid = Integer.parseInt(sendMessage);
		RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
		li = db.getMsgList(uid);
		
		if(li.isEmpty())
			li.add("No res found");
		
		final ListView list=(ListView) findViewById(R.id.listView23);
        Log.i("BritiList", "I am Here4");
      
        ArrayAdapter<String> adp=new ArrayAdapter<String>(getBaseContext(),android.R.layout.simple_list_item_1,li);
        Log.i("BritiList", "I am Here4"+adp.getItem(0));
        	
        list.setAdapter(adp);
        
        list.setOnItemClickListener(new OnItemClickListener() {
			@Override
            public void onItemClick(AdapterView<?> parent, View view,
               int position, long id) {
              
			//if(position==0) return;
		     Log.i("BritiDb", "list click");
             // ListView Clicked item index
             int itemPosition     = position;
             // ListView Clicked item value
              String  itemValue    = (String) list.getItemAtPosition(position);


              Log.i("BritiDb", "Start Call"+itemValue);
              
              RTSdb db = new RTSdb(RTSappContext.getInstance().getContext());
              List<String> msgList1 = db.getMsgOfColumn(uid,itemValue,0);
              List<String> msgList2 = db.getMsgOfColumn(uid,itemValue,1);
              String str = buildString(msgList1,msgList2);
            //------------- Call new Intent ---------------
              Log.i("BRITICHK", "FinalSr:-"+str);
              Intent intent = new Intent(DisplayResourceList.this, TimeView.class);
              intent.putExtra(EXTRA_MESSAGE, str);
              startActivity(intent);

			}
        });
	}

	private String buildString(List<String> str1,List<String> str2)
	{
		String finStr="";
		String strMsg1 = "";
		String strMsg2 = "";
		for(int i=0;i<str1.size();i++)
		{
			strMsg1+=str1.get(i);
			strMsg2+=str2.get(i);
		}
		
		finStr = strMsg1+"#"+strMsg2;
		
		return finStr;
	}

}
