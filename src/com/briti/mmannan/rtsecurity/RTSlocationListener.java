package com.briti.mmannan.rtsecurity;
import android.location.Location;

public interface RTSlocationListener {
	void onLocationChanged(Location location);
}

