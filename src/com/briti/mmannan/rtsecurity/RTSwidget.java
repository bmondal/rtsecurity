package com.briti.mmannan.rtsecurity;



import java.util.ArrayList;
import java.util.List;

import android.appwidget.AppWidgetProviderInfo;
import android.os.Binder;
import android.util.Log;

public class RTSwidget extends RIntercept {
	private Methods mMethod;

	private RTSwidget(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.appwidget.AppWidgetManager";
	}

	// public List<AppWidgetProviderInfo> getInstalledProviders()
	// frameworks/base/core/java/android/appwidget/AppWidgetManager.java
	// http://developer.android.com/reference/android/appwidget/AppWidgetManager.html

	private enum Methods {
		getInstalledProviders
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSwidget(Methods.getInstalledProviders, "widget"));
		return listHook;
	}

	@Override
	protected void before(RObject obj) throws Throwable {
		if (mMethod == Methods.getInstalledProviders) {
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getInstalledProviders","widget");
		}
	}

	@Override
	protected void after(RObject obj) throws Throwable {
	
	}
	
	 protected void insertIntoDB(int uid,String method,String rsc) {
			
			String sndMsg = uid+"-"+method+"-"+rsc;
			RTSClient cl = new RTSClient(sndMsg);
			cl.sendMessage();
		}
}
