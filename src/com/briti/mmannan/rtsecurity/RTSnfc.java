package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;


import android.os.Binder;


public class RTSnfc extends RIntercept {
	private Methods mMethod;

	protected RTSnfc(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	@Override
	public String getClassName() {
		return "android.nfc.NfcAdapter";
	}

	private enum Methods {
		getDefaultAdapter, getNfcAdapter
	};

	// public static NfcAdapter getDefaultAdapter() [deprecated]
	// public static NfcAdapter getDefaultAdapter(Context context)
	// public static synchronized NfcAdapter getNfcAdapter(Context context)
	// frameworks/base/core/java/android/nfc/NfcAdapter.java
	// http://developer.android.com/reference/android/nfc/NfcAdapter.html

	// NfcManager.getDefaultAdapter calls NfcAdapter.getNfcAdapter
	// http://developer.android.com/reference/android/nfc/NfcManager.html

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSnfc(Methods.getDefaultAdapter, "nfc"));
		listHook.add(new RTSnfc(Methods.getNfcAdapter, "nfc"));
		return listHook;
	}

	@Override
	protected void before(RObject obj) throws Throwable {
		int uid = Binder.getCallingUid();

		if (mMethod == Methods.getDefaultAdapter) 
			insertIntoDB(uid,"getDefaultAdapter","nfc");
		if(mMethod == Methods.getNfcAdapter) 
			insertIntoDB(uid,"getNfcAdapter","nfc");
	}

	@Override
	protected void after(RObject obj) throws Throwable {
		// Do nothing
	}
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
