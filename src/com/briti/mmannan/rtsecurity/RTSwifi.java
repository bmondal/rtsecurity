package com.briti.mmannan.rtsecurity;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.net.DhcpInfo;
import android.net.wifi.ScanResult;
import android.net.wifi.SupplicantState;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.os.Binder;
import android.util.Log;

public class RTSwifi extends RIntercept {
	private Methods mMethod;
	private String mClassName;

	private RTSwifi(Methods method, String restrictionName, String className) {
		super(restrictionName, method.name(), null);
		mMethod = method;
		mClassName = className;
	}

	public String getClassName() {
		return mClassName;
	}

	// public List<WifiConfiguration> getConfiguredNetworks()
	// public WifiInfo getConnectionInfo()
	// public DhcpInfo getDhcpInfo()
	// public List<ScanResult> getScanResults()
	// public WifiConfiguration getWifiApConfiguration()
	// frameworks/base/wifi/java/android/net/wifi/WifiManager.java
	// frameworks/base/wifi/java/android/net/wifi/WifiInfo.java
	// frameworks/base/core/java/android/net/DhcpInfo.java
	// http://developer.android.com/reference/android/net/wifi/WifiManager.html

	private enum Methods {
		getConfiguredNetworks, getConnectionInfo, getDhcpInfo, getScanResults, getWifiApConfiguration
	};

	public static List<RIntercept> getInstances(Object instance) {
		String className = instance.getClass().getName();
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		for (Methods wifi : Methods.values())
			listHook.add(new RTSwifi(wifi, "network", className));

		listHook.add(new RTSwifi(Methods.getScanResults, "network", className));

		// This is to fake "offline", no permission required
		listHook.add(new RTSwifi(Methods.getConnectionInfo, "network", className));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		// Do nothing
	}

	@Override
	protected void after(RObject param) throws Throwable {
		
		if (mMethod == Methods.getConfiguredNetworks) {
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getConfiguredNetworks","wifi");

		} else if (mMethod == Methods.getConnectionInfo) {
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getConnectionInfo","wifi");
		} else if (mMethod == Methods.getDhcpInfo) {
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getDhcpInfo","wifi");
			
		} else if (mMethod == Methods.getScanResults) {
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getScanResults","wifi");

		} else if (mMethod == Methods.getWifiApConfiguration) {
			
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getWifiApConfiguration","wifi");

		} else{}
			
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
