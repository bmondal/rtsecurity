package com.briti.mmannan.rtsecurity;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class RTSappContext {

	private static RTSappContext   _instance;
    private Context appContext;
    private SQLiteDatabase mDb;
	private RTSappContext()
	{

	}

	public static RTSappContext getInstance()
	{
	    if (_instance == null)
	    {
	        _instance = new RTSappContext();
	    }
	    return _instance;
	}
	
	public void setContext(Context context)
	{
		appContext = context;
	}
	
	public Context getContext()
	{
		return appContext;
	}
	
	public void setDB(SQLiteDatabase db)
	{
		this.mDb = db;
	}
    public SQLiteDatabase getDB() {
		return this.mDb;
	}

}

