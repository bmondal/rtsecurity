package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class ParseMessage {

	private static ParseMessage   _instance = null;

    static List<String> msgList = new ArrayList<String>();
    
	private ParseMessage()
	{
		
	}
	public static ParseMessage getInstance()
	{
	    if (_instance == null)
	    {
	    	Log.i("BritiST", "For the first time");
	        _instance = new ParseMessage();
	    }
	    return _instance;
	}
	
	public void insertIntoMsgList(String msg)
	{
		Log.i("BritiDB", "msgList added");
		msgList.add(msg);
		Log.i("BritiDB", "chk message "+msgList.get(msgList.size()-1));
	}
	
	public List<String> getList(){ return msgList;}
	
	public String GetTopMessage() {
		Log.i("BritiDB", "Method called by thr "+msgList.size());
		if(msgList.size()>0)
		{
			Log.i("BritiDB", "Method found msg "+ msgList.get(msgList.size()-1));
			return msgList.get(msgList.size()-1);
		}
		return "";
		
	}
	
	
	public List<String> getListValueWithClearList()
	{
		List<String> tempList = new ArrayList<String>();
		if(!msgList.isEmpty())
		{
			Log.i("BritiTHRD", "msgList not Empty and clear");
			for(int i=0;i<msgList.size();i++)
				tempList.add(msgList.get(i));
			msgList.clear();
		}
		
		return tempList;
	}
	
}
