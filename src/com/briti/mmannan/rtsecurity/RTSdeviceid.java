package com.briti.mmannan.rtsecurity;

import java.util.ArrayList;
import java.util.List;

import android.os.Binder;
import android.provider.Settings;
import android.util.Log;

public class RTSdeviceid extends RIntercept {
	private Methods mMethod;

	private RTSdeviceid(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.provider.Settings.Secure";
	}

	// @formatter:off

	// public synchronized static String getString(ContentResolver resolver, String name)
	// frameworks/base/core/java/android/provider/Settings.java
	// frameworks/base/core/java/android/content/ContentResolver.java
	// http://developer.android.com/reference/android/provider/Settings.Secure.html

	// @formatter:on

	private enum Methods {
		getString
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSdeviceid(Methods.getString, "deviceid"));
		return listHook;
	}

	@Override
	protected void before(RObject obj) throws Throwable {
		if (mMethod == Methods.getString) {
			int uid = Binder.getCallingUid();
			insertIntoDB(uid,"getString","deviceid");
		}
	}

	@Override
	protected void after(RObject obj) throws Throwable {

	}
	
protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}
