package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.net.NetworkInfo;
import android.os.Binder;
import android.util.Log;

public class RTSmobileNetwork extends RIntercept {
	private Methods mMethod;

	private RTSmobileNetwork(Methods method, String restrictionName) {
		super(restrictionName, method.name(), null);
		mMethod = method;
	}

	public String getClassName() {
		return "android.net.NetworkInfo";
	}

	private enum Methods {
		getDetailedState, getExtraInfo, getState, isConnected, isConnectedOrConnecting
	};

	public static List<RIntercept> getInstances() {
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		for (Methods ninfo : Methods.values())
			listHook.add(new RTSmobileNetwork(ninfo,"internet"));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		// Do nothing
	}

	@Override
	protected void after(RObject param) throws Throwable {
		int uid = Binder.getCallingUid();
		//Reports the current fine-grained state of the network.
		if (mMethod == Methods.getDetailedState) {
			insertIntoDB(uid,"getDetailedState","network");

		}//Report the extra information about the network state, if any was provided by the lower networking layers., if one is available. 
		else if (mMethod == Methods.getExtraInfo) {
			insertIntoDB(uid,"getExtraInfo","network");
		}//Reports the current coarse-grained state of the network.
		else if (mMethod == Methods.getState) {
			insertIntoDB(uid,"getState","network");
		}//Indicates whether network connectivity exists or is in the process of being established. 
		else if (mMethod == Methods.isConnected || mMethod == Methods.isConnectedOrConnecting) {
			insertIntoDB(uid,"isConnectedOrConnecting","network");
		} else{}
			
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
			/*
			String sndMsg = uid+"-"+method+"-"+rsc;
			RTSClient cl = new RTSClient(sndMsg);
			cl.sendMessage();
			*/
	}
}
