package com.briti.mmannan.rtsecurity;


import java.util.ArrayList;
import java.util.List;

import android.hardware.Sensor;
import android.os.Binder;
import android.util.Log;

public class RTSsensor extends RIntercept {
	private Methods mMethod;
	private String mClassName;

	private RTSsensor(Methods method, String restrictionName, String className) {
		super(restrictionName, method.name(), null);
		mMethod = method;
		mClassName = className;
	}

	public String getClassName() {
		return mClassName;
	}

	// @formatter:off

	// public Sensor getDefaultSensor(int type)
	// public List<Sensor> getSensorList(int type)
	// frameworks/base/core/java/android/hardware/SensorManager.java
	// http://developer.android.com/reference/android/hardware/SensorManager.html
	
	// @formatter:on

	private enum Methods {
		getDefaultSensor, getSensorList
	};

	public static List<RIntercept> getInstances(Object instance) {
		String className = instance.getClass().getName();
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSsensor(Methods.getDefaultSensor, "sensor", className));
		listHook.add(new RTSsensor(Methods.getSensorList, "sensor", className));
		return listHook;
	}

	@Override
	protected void before(RObject param) throws Throwable {
		int uid = Binder.getCallingUid();
		if (mMethod == Methods.getDefaultSensor) {
			insertIntoDB(uid,"getDefaultSensor","sensor");
		} else if (mMethod == Methods.getSensorList) {
			insertIntoDB(uid,"getSensorList","sensor");
		} else{}
			
	}

	@Override
	protected void after(RObject param) throws Throwable {
		// Do nothing
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}
}

