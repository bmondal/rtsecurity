package com.briti.mmannan.rtsecurity;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.TimeUnit;


import android.accounts.Account;
import android.accounts.AccountManager;
import android.accounts.AccountManagerCallback;
import android.accounts.AccountManagerFuture;
import android.accounts.AuthenticatorDescription;
import android.accounts.AuthenticatorException;
import android.accounts.OnAccountsUpdateListener;
import android.accounts.OperationCanceledException;
import android.os.Binder;
import android.os.Bundle;
import android.util.Log;

public class RTSaccountHandler extends RIntercept {
	private Methods mMethod;
	private String mClassName;
	
	private RTSaccountHandler(Methods method, String restrictionName, String className) {
		super(restrictionName, method.name(), null);
		mMethod = method;
		mClassName = className;
	}

	private RTSaccountHandler(Methods method, String restrictionName, String className, int sdk) {
		super(restrictionName, method.name(), null, sdk);
		mMethod = method;
		mClassName = className;
	}

	public String getClassName() {
		return mClassName;
	}


	private enum Methods {
		getAccounts,
		getAuthToken
	};
	// @formatter:on

	public static List<RIntercept> getInstances(Object instance) {
		String className = instance.getClass().getName();
		List<RIntercept> listHook = new ArrayList<RIntercept>();
		listHook.add(new RTSaccountHandler(Methods.getAccounts, "account", className));
		listHook.add(new RTSaccountHandler(Methods.getAuthToken, "account", className));
		return listHook;
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void before(RObject param) throws Throwable {
		int uid = Binder.getCallingUid();
			
		if (mMethod == Methods.getAuthToken) {
			insertIntoDB(uid,"getAuthToken","account");
		}
		else if (mMethod == Methods.getAccounts) {
			insertIntoDB(uid,"getAccounts","account");
		}
	}

	@Override
	@SuppressWarnings("unchecked")
	protected void after(RObject param) throws Throwable {
		
	}
	
	protected void insertIntoDB(int uid,String method,String rsc) {
		
		String sndMsg = uid+"-"+method+"-"+rsc;
		RTSClient cl = new RTSClient(sndMsg);
		cl.sendMessage();
	}	

}

